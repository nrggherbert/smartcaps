﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Serialization;
namespace SMART
{
    public partial class frmMain : Form
    {

        private void getXML()
        {
            CAPSService.CAPSServiceClient client = new CAPSService.CAPSServiceClient();
            CAPSService.GetCAPSRequest getCAPSreq = new CAPSService.GetCAPSRequest();
            getCAPSreq.CAPSNode = "NRGFLEET";
            getCAPSreq.Password = "368553d99A0504B4";

            CAPSService.GetCAPSResp getCAPSResponse = new CAPSService.GetCAPSResp();
            //TODO put this back in : getCAPSResponse = client.GetCAPS(getCAPSreq);

            XmlDocument doc = new XmlDocument();
            FileStream fs = new FileStream("..\\..\\..\\GetCAPSExamples.xml", FileMode.Open);
            doc.Load(fs);
            fs.Close();
            getCAPSResponse.Payload = doc.InnerXml;

            caps_data cd = new caps_data();

            XmlSerializer reader = new XmlSerializer(typeof(caps_data));
            //            StreamReader streamReader = new StreamReader(doc.InnerXml.ToString());
            StreamReader streamReader = new StreamReader(@"..\..\..\GetCAPSExamples2.xml");
            cd = (caps_data)reader.Deserialize(streamReader);

            bool result = false;
            string sqlConnString = Properties.Settings.Default.Properties["SMARTdevConnectionString"].DefaultValue.ToString();
            string insertSQL;
            insertSQL = "INSERT INTO FNOLXML(CAPSxml) VALUES(@xml)";
            using (SqlConnection sqlConn = new SqlConnection(sqlConnString))
            {
                using (SqlCommand cmd = new SqlCommand(insertSQL, sqlConn))
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.AddWithValue("@xml", doc.InnerXml);

                    try
                    {
                        sqlConn.Open();
                        result = cmd.ExecuteNonQuery() == 1;

                        sqlConn.Close();
                    }
                    catch (Exception ex)
                    {
                        if (sqlConn.State == ConnectionState.Open)
                            sqlConn.Close();
                        streamReader.Close();
                        MessageBox.Show("Error : " + ex.Message);
                        return;
                    }
                }
            }
            streamReader.Close();

            // We now have the caps_data data in the object, need to strip it all out and save to the database
            // loop through all the transactions that we get back
            for (int trans = 0; trans < cd.Transaction.Length; trans++)
            {
                // get the registration so we can find the correct FNOL id
                string registration = cd.Transaction[trans].Job.VehicleDetail.VehicleReg.ToString().Trim();
                SqlConnection sqlConn = new SqlConnection(sqlConnString);
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = sqlConn;
                cmd.CommandText = "SELECT id FROM FNOL WHERE registration = '" + registration + "'";
                int fnoldID = 0;
                sqlConn.Open();
                SqlDataReader dataReader = cmd.ExecuteReader();
                dataReader.Read();

                fnoldID = Convert.ToInt32(dataReader["id"].ToString());
                dataReader.Close();

                Job j = new Job();
                j = cd.Transaction[trans].Job;
                // create the event object so we can load data into it
                Event evt = new Event();

                SqlCommand cmdInsertEvent = new SqlCommand();
                cmdInsertEvent.Connection = sqlConn;
                cmdInsertEvent.CommandType = CommandType.StoredProcedure;
                cmdInsertEvent.CommandText = "usp_FNOLInsertEvent";

                for (int evnts = 0; evnts < j.Events.Length - 1; evnts++)
                {
                    evt = j.Events[evnts];
                    cmdInsertEvent.Parameters.Clear();
                    cmdInsertEvent.Parameters.AddWithValue("@fnolID", fnoldID);
                    cmdInsertEvent.Parameters.AddWithValue("@eventName", evt.EventName.ToString());
                    cmdInsertEvent.Parameters.AddWithValue("@eventDateTime", Convert.ToDateTime(evt.EventDatetime));
                    cmdInsertEvent.ExecuteNonQuery();

                } // for evnts

                // create the process object so we can load data into it
                Process process = new Process();
                SqlCommand cmdInsertProcess = new SqlCommand();
                cmdInsertProcess.Connection = sqlConn;
                cmdInsertProcess.CommandType = CommandType.StoredProcedure;
                cmdInsertProcess.CommandText = "usp_FNOLInsertProcess";

                for (int proc = 0; proc < j.Processes.Length - 1; proc++)
                {
                    process = j.Processes[proc];
                    cmdInsertProcess.Parameters.Clear();
                    cmdInsertProcess.Parameters.AddWithValue("@fnolID", fnoldID);
                    cmdInsertProcess.Parameters.AddWithValue("@workshopTask", process.WorkshopTask.ToString());
                    cmdInsertProcess.Parameters.AddWithValue("@taskStatus", process.TaskStatus.ToString());
                    cmdInsertProcess.Parameters.AddWithValue("@progressPercentage", Convert.ToInt32(float.Parse(process.ProgressPercentage)));
                    cmdInsertProcess.Parameters.AddWithValue("@progressExpectedStartDate", Convert.ToDateTime(process.ProgressExpectedStartDate));
                    cmdInsertProcess.Parameters.AddWithValue("@progressCompletionDate", Convert.ToDateTime(process.ProgressCompletionDate));
                    cmdInsertProcess.ExecuteNonQuery();


                }// for processes

                ExternalCommunication communication = new ExternalCommunication();
                SqlCommand cmdInsertComm = new SqlCommand();
                cmdInsertComm.Connection = sqlConn;
                cmdInsertComm.CommandType = CommandType.StoredProcedure;
                cmdInsertComm.CommandText = "usp_FNOLInsertComm";

                for (int comm = 0; comm < j.ExternalCommunications.Length - 1; comm++)
                {
                    communication = j.ExternalCommunications[comm];
                    cmdInsertComm.Parameters.Clear();
                    cmdInsertComm.Parameters.AddWithValue("@fnolID", fnoldID);
                    cmdInsertComm.Parameters.AddWithValue("@communicationType", communication.CommunicationType);
                    cmdInsertComm.Parameters.AddWithValue("@communicationText", communication.CommunicationText);
                    cmdInsertComm.Parameters.AddWithValue("@communicationBy", communication.CommunicationBy);
                    cmdInsertComm.Parameters.AddWithValue("@communicationDateTime", Convert.ToDateTime(communication.CommunicationDatetime));
                    cmdInsertComm.ExecuteNonQuery();

                }

                EstimateDetail estimateDetail = new EstimateDetail();
                SqlCommand cmdInsertEstimate = new SqlCommand();
                cmdInsertEstimate.Connection = sqlConn;
                cmdInsertEstimate.CommandType = CommandType.StoredProcedure;
                cmdInsertEstimate.CommandText = "usp_FNOLInsertEstimate";

                estimateDetail = j.EstimateDetail;
                cmdInsertEstimate.Parameters.Clear();
                cmdInsertEstimate.Parameters.AddWithValue("@fnolID", fnoldID);
                cmdInsertEstimate.Parameters.AddWithValue("@totalLabourHours", estimateDetail.TotalLabourHours);
                cmdInsertEstimate.Parameters.AddWithValue("@totalMaterialsNet", estimateDetail.TotalMaterialsNet);
                cmdInsertEstimate.Parameters.AddWithValue("@totalPartsNet", estimateDetail.TotalPartsNet);
                cmdInsertEstimate.Parameters.AddWithValue("@totalOtherNet", estimateDetail.TotalOtherNet);
                cmdInsertEstimate.Parameters.AddWithValue("@totalNetOnEstimate", estimateDetail.TotalNetOnEstimate);
                cmdInsertEstimate.Parameters.AddWithValue("@estimatorName", estimateDetail.EstimatorName);
                cmdInsertEstimate.Parameters.AddWithValue("@partsEstimateCount", estimateDetail.PartsEstimateCount);
                cmdInsertEstimate.Parameters.AddWithValue("@partsOrderedCount", estimateDetail.PartsOrderedCount);
                cmdInsertEstimate.Parameters.AddWithValue("@partReceivedCount", estimateDetail.PartsReceivedCount);
                cmdInsertEstimate.ExecuteNonQuery();

                sqlConn.Close();

            } // for trans

            /*
                                    XmlDocument xmldoc = new XmlDocument();
                                    XmlNode xmlnode;
                                    //XmlElement root = doc.DocumentElement;
                        //            xmldoc.Load(streamReader);
                                    xmldoc.LoadXml(doc.InnerXml.ToString());

                                    treeView1.Nodes.Clear();
                                    xmlnode = xmldoc.ChildNodes[1];
                                    treeView1.Nodes.Add(new TreeNode(xmldoc.DocumentElement.Name));
                                    TreeNode tNode;
                                    tNode = treeView1.Nodes[0];
                                    AddNode(xmlnode, tNode);
            */

            /*
                        caps_data cd = new caps_data();
                        XmlSerializer xsSubmit = new XmlSerializer(typeof(caps_data));
                        var xml = "";
                        using (var sww = new StringWriter())
                        {
                            using (System.Xml.XmlWriter writer = XmlWriter.Create(sww))
                            {
                                xsSubmit.Serialize(writer, cd);
                                xml = sww.ToString();
                            }
                        }

            */



        }
    }

}