﻿using System.Data;
using System.IO;
using System.Net;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Serialization;



namespace SMART
{
    public partial class frmNewFNOL : Form
    {

        private void createXML(int rowNum)
        {
            // read the correct row from the table
            DataTable dt = dsFNOL.Tables["FNOL"];
            DataRow dr = dt.Rows.Find(rowNum);

            // create the CAPS object
            caps_data cd = new caps_data();
            TransmissionHeader th = new TransmissionHeader();
            th.TransmissionOriginType = TransmissionOriginType.EXTERNALAPPLICATION;
            th.TransmissionOriginCode = "NRGFLEET";

            TransactionHeader transheader = new TransactionHeader();
            transheader.TransactionOriginCode = "NRGFLEET";
            transheader.TransactionOriginType = TransactionOriginType.EXTERNALAPPLICATION;

            JobOffer joboffer = new JobOffer();
            joboffer.OfferStatus = OfferStatus.NEW;
            transheader.JobOffer = joboffer;

            cd.TransmissionHeader = th;

            VehicleDetail vd = new VehicleDetail();

            vd.VehicleReg = ((SMART.dsFNOL.FNOLRow)dr).registration.ToString();
            //TODO vd.VehicleMake = 
            vd.VehicleMake = "MITSUBISHI";

            Job j = new Job();
            j.VehicleDetail = vd;
            JobDetail jd = new JobDetail();
            jd.JobOriginType = JobOriginType.EXTERNALAPPLICATION;
            jd.JobOriginCode = "NRGFLEET";
            jd.SPJobID = "TBA";

            jd.JobOriginType = JobOriginType.WORKPROVIDER;
            //TODO jobDetail.JobOriginCode = 
            jd.SPJobID = "Unknown";
            jd.WPCaseRef = "Unknown";
            //TODO jobDetail.PolicyNo = 
            jd.PolicyNo = "666999";
            //TODO jobDetail.ClaimNo = 
            jd.ClaimNo = "999666";

            Transaction[] t = new Transaction[1];

            t[0] = new Transaction();
            j.JobDetail = jd;
            t[0].Job = j;
            //TODO use service provider from selection on screen
            transheader.ServiceProviderCode = "_DX_ACE_GALWAY";
            transheader.InsurerCode = "MFS_OTHER";

            t[0].TransactionHeader = transheader;

            //TODO transheader.InsurerCode = <insurance code>;
            Interests interests = new Interests();
            Interest[] interest = new Interest[1];
            interest[0] = new Interest();
            interest[0].InterestType = new InterestType();
            interest[0].InterestType = InterestType.EXTERNALAPPLICATION;
            //TODO interest[0].InterestCode = 
            //TODO interest[0].InterestJobRef = 

            cd.Transaction = t;

            TransmissionTrailer tt = new TransmissionTrailer();
            tt.TransmissionChecksum = "ABCDEF";
            cd.TransmissionTrailer = tt;

            // create the XML
            XmlSerializer xsSubmit = new XmlSerializer(typeof(caps_data));
            var xml = "";

            XmlWriterSettings xmlWriterSettings = new XmlWriterSettings
            {
                Encoding = Encoding.UTF8
            };

            using (var sww = new StringWriter())
            {
                using (System.Xml.XmlWriter writer = XmlWriter.Create(sww, xmlWriterSettings))
                {
                    xsSubmit.Serialize(writer, cd);
                    xml = sww.ToString();
                }
            }

            // create the client
            CAPSService.CAPSServiceClient client = new CAPSService.CAPSServiceClient();

            // create the request
            CAPSService.PutCAPSRequest putCAPSRequest = new CAPSService.PutCAPSRequest();

            putCAPSRequest.RespMode = "R";
            putCAPSRequest.CAPSNode = "NRGFLEET";
            putCAPSRequest.Password = "368553d99A0504B4";
//            putCAPSRequest.Password = "S1xt33nCh4r4ct3rs";
            
            // allocate all the data from the xml
            putCAPSRequest.Payload = xml;

            // create the response to obtain the return from the request
            CAPSService.PutCAPSResp putCAPSResp = new CAPSService.PutCAPSResp();

            // send to CAPS
            putCAPSResp = client.PutCAPS(putCAPSRequest);

            if (putCAPSResp.StatusCode == "0")
            {
                MessageBox.Show("Sent to CAPS successfully");
            }
            else
            {
                MessageBox.Show("Error sending to CAPS.  Return message is : " + putCAPSResp.StatusDesc.ToString());
            }

            //capsService.PutCAPSRequest putCAPS = new capsService.PutCAPSRequest();

            //putCAPS.Password = "S1xt33nCh4r4ct3rs";
            //putCAPS.Payload = xml;

            //            CAPSServiceClient client = new CAPSServiceClient();


            /*
                        //CAPSServiceClient client = new CAPSServiceClient();
                        PutCAPSRequest pcr = new PutCAPSRequest();
                        pcr.Password = "S1xt33nCh4r4ct3rs";
                        pcr.Payload = xml;

                        PutCAPSResp resp = new PutCAPSResp();
                        resp = client.PutCAPS(pcr);
                        string capsURL = "http://service.caps.co.uk/CWS/server";
                        WebRequest wr = WebRequest.Create(capsURL);
                        HttpWebResponse resp = (HttpWebResponse)wr.GetResponse();
                        Stream dataStream = resp.GetResponseStream();
                        StreamReader reader = new StreamReader(dataStream);
                        string serverResponse = reader.ReadToEnd();
                        reader.Close();
                        dataStream.Close();
                        resp.Close();
            */

        }
    }
}