﻿using System.Windows.Forms;

namespace SMART
{
    public partial class frmShowCAPS : Form
    {

        /*
       private void AddNode(XmlNode inXmlNode, TreeNode inTreeNode)
       {
           XmlNode xNode;
           TreeNode tNode;
           XmlNodeList nodeList;
           int i = 0;
           if (inXmlNode.HasChildNodes)
           {
               nodeList = inXmlNode.ChildNodes;
               for (i = 0; i <= nodeList.Count - 1; i++)
               {
                   xNode = inXmlNode.ChildNodes[i];
                   inTreeNode.Nodes.Add(new TreeNode(xNode.Name));
                   tNode = inTreeNode.Nodes[i];
                   AddNode(xNode, tNode);
               }
           }
           else
           {
               inTreeNode.Text = inXmlNode.InnerText.ToString();
           }
       }
*/
    }
}


/*
 * This is the layout of the data we need 
 * cap_data
	* transaction
		job
			Events
				*Event
					Event-Name
					Event-Datetime
			Processes
				*Process
					Workshop-Task
					Task-Status
					Progress-Percentage
					Progress-Expected-Start-Date
					Progress-Completion-Date
			External-Communications
				*External-Communication
					Communication-Type
					Communication-Text
					Communication-By
					Communication-Datetime
			Esimate-Detail
			          <Total-Labour-Hours>18.80</Total-Labour-Hours>
			          <Total-Labour-Net>542.00</Total-Labour-Net>
			          <Total-Materials-Net>393.46</Total-Materials-Net>
			          <Total-Parts-Net>0.00</Total-Parts-Net>
			          <Total-Other-Net>35.34</Total-Other-Net>
			          <Total-Net-On-Estimate>970.80</Total-Net-On-Estimate>
			          <Estimator-Name>ALFIE MANLEY</Estimator-Name>
			          <Parts-Estimate-Count>0</Parts-Estimate-Count>
			          <Parts-Ordered-Count>0</Parts-Ordered-Count>
			          <Parts-Received-Count>0</Parts-Received-Count>

				
*/