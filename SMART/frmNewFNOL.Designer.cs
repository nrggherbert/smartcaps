﻿namespace SMART
{
    partial class frmNewFNOL
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label registrationFreeLabel;
            this.wizardControl1 = new DevExpress.XtraWizard.WizardControl();
            this.wizardPage1 = new DevExpress.XtraWizard.WizardPage();
            this.registrationFreeTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.fNOLBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dsFNOL = new SMART.dsFNOL();
            this.cbRegNotFound = new System.Windows.Forms.CheckBox();
            this.dataLayoutControl1 = new DevExpress.XtraDataLayout.DataLayoutControl();
            this.is3rdPartyInvolvedCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.isBodyDamageCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.isEngineDamageCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.isWheelDamageCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.isFireDamageCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.isDriveableCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.damageDescriptionTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.driverNameTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.registrationTextEdit = new DevExpress.XtraEditors.LookUpEdit();
            this.vwRegistrationsBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.dsRegistrations = new SMART.dsRegistrations();
            this.driverPhoneNumberSpinEdit = new DevExpress.XtraEditors.TextEdit();
            this.customerIDSpinEdit = new DevExpress.XtraEditors.LookUpEdit();
            this.vwCustomersBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dsCustomers = new SMART.dsCustomers();
            this.dsFNOLBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
            this.completionWizardPage1 = new DevExpress.XtraWizard.CompletionWizardPage();
            this.wizardPage2 = new DevExpress.XtraWizard.WizardPage();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.otherNotesTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.isDriverAtFaultCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.locationTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.incidentDetailsTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.dateOfIncidentDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.eventNameTextEdit = new DevExpress.XtraEditors.LookUpEdit();
            this.vwEventTypesBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.dsEventTypesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dsEventTypes = new SMART.dsEventTypes();
            this.timeofIncidentTimeEdit = new DevExpress.XtraEditors.TimeEdit();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem13 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem15 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem16 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem17 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem18 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem19 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem21 = new DevExpress.XtraLayout.LayoutControlItem();
            this.wizardPage3 = new DevExpress.XtraWizard.WizardPage();
            this.layoutControl2 = new DevExpress.XtraLayout.LayoutControl();
            this.no3rdPartyVehiclesSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.supportingPhotosCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.speedSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.weatherConditionsSpinEdit = new DevExpress.XtraEditors.LookUpEdit();
            this.roadConditionsSpinEdit = new DevExpress.XtraEditors.LookUpEdit();
            this.layoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem14 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem20 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem22 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem23 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem24 = new DevExpress.XtraLayout.LayoutControlItem();
            this.vwEventTypesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.vwRegistrationsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dsRegistrationsBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.fNOLTableAdapter = new SMART.dsFNOLTableAdapters.FNOLTableAdapter();
            this.tableAdapterManager = new SMART.dsFNOLTableAdapters.TableAdapterManager();
            this.dsRegistrationsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.vwRegistrationsTableAdapter = new SMART.dsRegistrationsTableAdapters.vwRegistrationsTableAdapter();
            this.vw_CustomersTableAdapter = new SMART.dsCustomersTableAdapters.vw_CustomersTableAdapter();
            this.behaviorManager1 = new DevExpress.Utils.Behaviors.BehaviorManager(this.components);
            this.tableAdapterManager1 = new SMART.dsRegistrationsTableAdapters.TableAdapterManager();
            this.vwEventTypesTableAdapter = new SMART.dsEventTypesTableAdapters.vwEventTypesTableAdapter();
            this.tableAdapterManager2 = new SMART.dsEventTypesTableAdapters.TableAdapterManager();
            this.wizardPage4 = new DevExpress.XtraWizard.WizardPage();
            this.layoutControl3 = new DevExpress.XtraLayout.LayoutControl();
            this.layoutControlGroup5 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.isDriverInjuredCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.layoutControlItem25 = new DevExpress.XtraLayout.LayoutControlItem();
            this.driverInjuryDetailsTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.layoutControlItem26 = new DevExpress.XtraLayout.LayoutControlItem();
            this.treatmentReceivedCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.layoutControlItem27 = new DevExpress.XtraLayout.LayoutControlItem();
            this.isHospitalNeededCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.layoutControlItem28 = new DevExpress.XtraLayout.LayoutControlItem();
            this.hospitalDetailsTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.layoutControlItem29 = new DevExpress.XtraLayout.LayoutControlItem();
            this.wizardPage5 = new DevExpress.XtraWizard.WizardPage();
            this.layoutControl4 = new DevExpress.XtraLayout.LayoutControl();
            this.layoutControlGroup6 = new DevExpress.XtraLayout.LayoutControlGroup();
            this._3rdPartyRegTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.layoutControlItem30 = new DevExpress.XtraLayout.LayoutControlItem();
            this._3rdPartynameTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.layoutControlItem31 = new DevExpress.XtraLayout.LayoutControlItem();
            this._3rdPartyPhoneSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.layoutControlItem32 = new DevExpress.XtraLayout.LayoutControlItem();
            this._3rdPartyDamageDescriptionTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.layoutControlItem33 = new DevExpress.XtraLayout.LayoutControlItem();
            this._3rdPartyIsInjuredCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.layoutControlItem34 = new DevExpress.XtraLayout.LayoutControlItem();
            this._3rdPartyInjuryDetailsTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.layoutControlItem35 = new DevExpress.XtraLayout.LayoutControlItem();
            this._3rdPartyMakeTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.layoutControlItem36 = new DevExpress.XtraLayout.LayoutControlItem();
            this._3rdPartyModelTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.layoutControlItem37 = new DevExpress.XtraLayout.LayoutControlItem();
            this._3rdPartyInsuranceTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.layoutControlItem38 = new DevExpress.XtraLayout.LayoutControlItem();
            registrationFreeLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.wizardControl1)).BeginInit();
            this.wizardControl1.SuspendLayout();
            this.wizardPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.registrationFreeTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fNOLBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsFNOL)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).BeginInit();
            this.dataLayoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.is3rdPartyInvolvedCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.isBodyDamageCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.isEngineDamageCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.isWheelDamageCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.isFireDamageCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.isDriveableCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.damageDescriptionTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.driverNameTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.registrationTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vwRegistrationsBindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsRegistrations)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.driverPhoneNumberSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.customerIDSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vwCustomersBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsCustomers)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsFNOLBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).BeginInit();
            this.wizardPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.otherNotesTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.isDriverAtFaultCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.locationTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.incidentDetailsTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateOfIncidentDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateOfIncidentDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.eventNameTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vwEventTypesBindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsEventTypesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsEventTypes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.timeofIncidentTimeEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem21)).BeginInit();
            this.wizardPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).BeginInit();
            this.layoutControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.no3rdPartyVehiclesSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.supportingPhotosCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.speedSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.weatherConditionsSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.roadConditionsSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vwEventTypesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vwRegistrationsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsRegistrationsBindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsRegistrationsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.behaviorManager1)).BeginInit();
            this.wizardPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl3)).BeginInit();
            this.layoutControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.isDriverInjuredCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.driverInjuryDetailsTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.treatmentReceivedCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.isHospitalNeededCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.hospitalDetailsTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem29)).BeginInit();
            this.wizardPage5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl4)).BeginInit();
            this.layoutControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._3rdPartyRegTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._3rdPartynameTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem31)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._3rdPartyPhoneSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem32)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._3rdPartyDamageDescriptionTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem33)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._3rdPartyIsInjuredCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem34)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._3rdPartyInjuryDetailsTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem35)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._3rdPartyMakeTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem36)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._3rdPartyModelTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem37)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._3rdPartyInsuranceTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem38)).BeginInit();
            this.SuspendLayout();
            // 
            // registrationFreeLabel
            // 
            registrationFreeLabel.AutoSize = true;
            registrationFreeLabel.Location = new System.Drawing.Point(798, 22);
            registrationFreeLabel.Name = "registrationFreeLabel";
            registrationFreeLabel.Size = new System.Drawing.Size(79, 17);
            registrationFreeLabel.TabIndex = 3;
            registrationFreeLabel.Text = "Enter here:";
            // 
            // wizardControl1
            // 
            this.wizardControl1.Controls.Add(this.wizardPage1);
            this.wizardControl1.Controls.Add(this.completionWizardPage1);
            this.wizardControl1.Controls.Add(this.wizardPage2);
            this.wizardControl1.Controls.Add(this.wizardPage3);
            this.wizardControl1.Controls.Add(this.wizardPage4);
            this.wizardControl1.Controls.Add(this.wizardPage5);
            this.wizardControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.wizardControl1.Location = new System.Drawing.Point(0, 0);
            this.wizardControl1.LookAndFeel.SkinName = "Office 2013 Dark Gray";
            this.wizardControl1.Margin = new System.Windows.Forms.Padding(4);
            this.wizardControl1.MinimumSize = new System.Drawing.Size(133, 123);
            this.wizardControl1.Name = "wizardControl1";
            this.wizardControl1.Pages.AddRange(new DevExpress.XtraWizard.BaseWizardPage[] {
            this.wizardPage1,
            this.wizardPage2,
            this.wizardPage4,
            this.wizardPage3,
            this.wizardPage5,
            this.completionWizardPage1});
            this.wizardControl1.Size = new System.Drawing.Size(1122, 546);
            this.wizardControl1.Text = "Create New FNOL Record";
            this.wizardControl1.WizardStyle = DevExpress.XtraWizard.WizardStyle.WizardAero;
            this.wizardControl1.SelectedPageChanging += new DevExpress.XtraWizard.WizardPageChangingEventHandler(this.wizardControl1_SelectedPageChanging);
            this.wizardControl1.CancelClick += new System.ComponentModel.CancelEventHandler(this.wizardControl1_CancelClick);
            this.wizardControl1.FinishClick += new System.ComponentModel.CancelEventHandler(this.wizardControl1_FinishClick);
            this.wizardControl1.NextClick += new DevExpress.XtraWizard.WizardCommandButtonClickEventHandler(this.wizardControl1_NextClick);
            this.wizardControl1.Click += new System.EventHandler(this.wizardControl1_Click);
            // 
            // wizardPage1
            // 
            this.wizardPage1.Controls.Add(registrationFreeLabel);
            this.wizardPage1.Controls.Add(this.registrationFreeTextEdit);
            this.wizardPage1.Controls.Add(this.cbRegNotFound);
            this.wizardPage1.Controls.Add(this.dataLayoutControl1);
            this.wizardPage1.Margin = new System.Windows.Forms.Padding(4);
            this.wizardPage1.Name = "wizardPage1";
            this.wizardPage1.Size = new System.Drawing.Size(1062, 378);
            this.wizardPage1.Text = "Vehicle & Driver Details";
            this.wizardPage1.Click += new System.EventHandler(this.wizardPage1_Click);
            // 
            // registrationFreeTextEdit
            // 
            this.registrationFreeTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.fNOLBindingSource, "registration", true));
            this.registrationFreeTextEdit.Enabled = false;
            this.registrationFreeTextEdit.Location = new System.Drawing.Point(883, 20);
            this.registrationFreeTextEdit.Name = "registrationFreeTextEdit";
            this.registrationFreeTextEdit.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.registrationFreeTextEdit.Properties.MaxLength = 10;
            this.registrationFreeTextEdit.Size = new System.Drawing.Size(100, 20);
            this.registrationFreeTextEdit.TabIndex = 3;
            this.registrationFreeTextEdit.Validating += new System.ComponentModel.CancelEventHandler(this.registrationFreeTextEdit_Validating);
            // 
            // fNOLBindingSource
            // 
            this.fNOLBindingSource.DataMember = "FNOL";
            this.fNOLBindingSource.DataSource = this.dsFNOL;
            // 
            // dsFNOL
            // 
            this.dsFNOL.DataSetName = "dsFNOL";
            this.dsFNOL.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // cbRegNotFound
            // 
            this.cbRegNotFound.AutoSize = true;
            this.cbRegNotFound.Location = new System.Drawing.Point(656, 21);
            this.cbRegNotFound.Name = "cbRegNotFound";
            this.cbRegNotFound.Size = new System.Drawing.Size(144, 21);
            this.cbRegNotFound.TabIndex = 2;
            this.cbRegNotFound.Text = "Not in the system?";
            this.cbRegNotFound.UseVisualStyleBackColor = true;
            this.cbRegNotFound.CheckedChanged += new System.EventHandler(this.cbRegNotFound_CheckedChanged);
            // 
            // dataLayoutControl1
            // 
            this.dataLayoutControl1.Controls.Add(this.is3rdPartyInvolvedCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.isBodyDamageCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.isEngineDamageCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.isWheelDamageCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.isFireDamageCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.isDriveableCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.damageDescriptionTextEdit);
            this.dataLayoutControl1.Controls.Add(this.driverNameTextEdit);
            this.dataLayoutControl1.Controls.Add(this.registrationTextEdit);
            this.dataLayoutControl1.Controls.Add(this.driverPhoneNumberSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.customerIDSpinEdit);
            this.dataLayoutControl1.DataMember = "FNOL";
            this.dataLayoutControl1.DataSource = this.dsFNOLBindingSource;
            this.dataLayoutControl1.Location = new System.Drawing.Point(38, 4);
            this.dataLayoutControl1.Name = "dataLayoutControl1";
            this.dataLayoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(994, 183, 450, 400);
            this.dataLayoutControl1.Root = this.layoutControlGroup1;
            this.dataLayoutControl1.Size = new System.Drawing.Size(612, 281);
            this.dataLayoutControl1.TabIndex = 0;
            this.dataLayoutControl1.Text = "dataLayoutControl1";
            // 
            // is3rdPartyInvolvedCheckEdit
            // 
            this.is3rdPartyInvolvedCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.fNOLBindingSource, "is3rdPartyInvolved", true));
            this.is3rdPartyInvolvedCheckEdit.Location = new System.Drawing.Point(12, 250);
            this.is3rdPartyInvolvedCheckEdit.Name = "is3rdPartyInvolvedCheckEdit";
            this.is3rdPartyInvolvedCheckEdit.Properties.Caption = "A 3rd party is involved";
            this.is3rdPartyInvolvedCheckEdit.Size = new System.Drawing.Size(588, 19);
            this.is3rdPartyInvolvedCheckEdit.StyleController = this.dataLayoutControl1;
            this.is3rdPartyInvolvedCheckEdit.TabIndex = 13;
            this.is3rdPartyInvolvedCheckEdit.CheckedChanged += new System.EventHandler(this.is3rdPartyInvolvedCheckEdit_CheckedChanged);
            // 
            // isBodyDamageCheckEdit
            // 
            this.isBodyDamageCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.fNOLBindingSource, "isBodyDamage", true));
            this.isBodyDamageCheckEdit.Location = new System.Drawing.Point(12, 227);
            this.isBodyDamageCheckEdit.Name = "isBodyDamageCheckEdit";
            this.isBodyDamageCheckEdit.Properties.Caption = "Vehicle has body damage";
            this.isBodyDamageCheckEdit.Size = new System.Drawing.Size(588, 19);
            this.isBodyDamageCheckEdit.StyleController = this.dataLayoutControl1;
            this.isBodyDamageCheckEdit.TabIndex = 12;
            // 
            // isEngineDamageCheckEdit
            // 
            this.isEngineDamageCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.fNOLBindingSource, "isEngineDamage", true));
            this.isEngineDamageCheckEdit.Location = new System.Drawing.Point(12, 204);
            this.isEngineDamageCheckEdit.Name = "isEngineDamageCheckEdit";
            this.isEngineDamageCheckEdit.Properties.Caption = "Vehicle has engine damage";
            this.isEngineDamageCheckEdit.Size = new System.Drawing.Size(588, 19);
            this.isEngineDamageCheckEdit.StyleController = this.dataLayoutControl1;
            this.isEngineDamageCheckEdit.TabIndex = 11;
            // 
            // isWheelDamageCheckEdit
            // 
            this.isWheelDamageCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.fNOLBindingSource, "isWheelDamage", true));
            this.isWheelDamageCheckEdit.Location = new System.Drawing.Point(12, 181);
            this.isWheelDamageCheckEdit.Name = "isWheelDamageCheckEdit";
            this.isWheelDamageCheckEdit.Properties.Caption = "Vehicle has wheel damage";
            this.isWheelDamageCheckEdit.Size = new System.Drawing.Size(588, 19);
            this.isWheelDamageCheckEdit.StyleController = this.dataLayoutControl1;
            this.isWheelDamageCheckEdit.TabIndex = 10;
            // 
            // isFireDamageCheckEdit
            // 
            this.isFireDamageCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.fNOLBindingSource, "isFireDamage", true));
            this.isFireDamageCheckEdit.Location = new System.Drawing.Point(12, 158);
            this.isFireDamageCheckEdit.Name = "isFireDamageCheckEdit";
            this.isFireDamageCheckEdit.Properties.Caption = "Vehicle has fire damage";
            this.isFireDamageCheckEdit.Size = new System.Drawing.Size(588, 19);
            this.isFireDamageCheckEdit.StyleController = this.dataLayoutControl1;
            this.isFireDamageCheckEdit.TabIndex = 9;
            // 
            // isDriveableCheckEdit
            // 
            this.isDriveableCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.fNOLBindingSource, "isDriveable", true));
            this.isDriveableCheckEdit.Location = new System.Drawing.Point(12, 135);
            this.isDriveableCheckEdit.Name = "isDriveableCheckEdit";
            this.isDriveableCheckEdit.Properties.Caption = "Vehicle is driveable";
            this.isDriveableCheckEdit.Size = new System.Drawing.Size(588, 19);
            this.isDriveableCheckEdit.StyleController = this.dataLayoutControl1;
            this.isDriveableCheckEdit.TabIndex = 8;
            // 
            // damageDescriptionTextEdit
            // 
            this.damageDescriptionTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.fNOLBindingSource, "damageDescription", true));
            this.damageDescriptionTextEdit.Location = new System.Drawing.Point(121, 111);
            this.damageDescriptionTextEdit.Name = "damageDescriptionTextEdit";
            this.damageDescriptionTextEdit.Size = new System.Drawing.Size(479, 20);
            this.damageDescriptionTextEdit.StyleController = this.dataLayoutControl1;
            this.damageDescriptionTextEdit.TabIndex = 7;
            // 
            // driverNameTextEdit
            // 
            this.driverNameTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.fNOLBindingSource, "driverName", true));
            this.driverNameTextEdit.Location = new System.Drawing.Point(121, 63);
            this.driverNameTextEdit.Name = "driverNameTextEdit";
            this.driverNameTextEdit.Size = new System.Drawing.Size(479, 20);
            this.driverNameTextEdit.StyleController = this.dataLayoutControl1;
            this.driverNameTextEdit.TabIndex = 5;
            // 
            // registrationTextEdit
            // 
            this.registrationTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.fNOLBindingSource, "registration", true));
            this.registrationTextEdit.Location = new System.Drawing.Point(121, 15);
            this.registrationTextEdit.Name = "registrationTextEdit";
            this.registrationTextEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.registrationTextEdit.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Customer_Fleetlist_VehicleReg", "Customer_Fleetlist_Vehicle Reg", 173, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.registrationTextEdit.Properties.DataSource = this.vwRegistrationsBindingSource1;
            this.registrationTextEdit.Properties.DisplayMember = "Customer_Fleetlist_VehicleReg";
            this.registrationTextEdit.Properties.DropDownRows = 20;
            this.registrationTextEdit.Properties.NullText = "";
            this.registrationTextEdit.Properties.ValueMember = "Customer_FleetListID";
            this.registrationTextEdit.Size = new System.Drawing.Size(479, 20);
            this.registrationTextEdit.StyleController = this.dataLayoutControl1;
            this.registrationTextEdit.TabIndex = 1;
            this.registrationTextEdit.Validating += new System.ComponentModel.CancelEventHandler(this.registrationTextEdit_Validating);
            // 
            // vwRegistrationsBindingSource1
            // 
            this.vwRegistrationsBindingSource1.DataMember = "vwRegistrations";
            this.vwRegistrationsBindingSource1.DataSource = this.dsRegistrations;
            // 
            // dsRegistrations
            // 
            this.dsRegistrations.DataSetName = "dsRegistrations";
            this.dsRegistrations.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // driverPhoneNumberSpinEdit
            // 
            this.driverPhoneNumberSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.fNOLBindingSource, "driverPhoneNumber", true));
            this.driverPhoneNumberSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.driverPhoneNumberSpinEdit.Location = new System.Drawing.Point(121, 87);
            this.driverPhoneNumberSpinEdit.Name = "driverPhoneNumberSpinEdit";
            this.driverPhoneNumberSpinEdit.Properties.Appearance.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.driverPhoneNumberSpinEdit.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.driverPhoneNumberSpinEdit.Properties.EditValueChangedFiringMode = DevExpress.XtraEditors.Controls.EditValueChangedFiringMode.Buffered;
            this.driverPhoneNumberSpinEdit.Size = new System.Drawing.Size(479, 20);
            this.driverPhoneNumberSpinEdit.StyleController = this.dataLayoutControl1;
            this.driverPhoneNumberSpinEdit.TabIndex = 6;
            this.driverPhoneNumberSpinEdit.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.driverPhoneNumberSpinEdit_KeyPress);
            this.driverPhoneNumberSpinEdit.Validating += new System.ComponentModel.CancelEventHandler(this.driverPhoneNumberSpinEdit_Validating);
            // 
            // customerIDSpinEdit
            // 
            this.customerIDSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.fNOLBindingSource, "customerID", true));
            this.customerIDSpinEdit.Location = new System.Drawing.Point(121, 39);
            this.customerIDSpinEdit.Name = "customerIDSpinEdit";
            this.customerIDSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.customerIDSpinEdit.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Customer_Loc_Name", "Customer_Loc_Name", 111, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.customerIDSpinEdit.Properties.DataSource = this.vwCustomersBindingSource;
            this.customerIDSpinEdit.Properties.DisplayMember = "Customer_Loc_Name";
            this.customerIDSpinEdit.Properties.DropDownRows = 20;
            this.customerIDSpinEdit.Properties.EditValueChangedFiringMode = DevExpress.XtraEditors.Controls.EditValueChangedFiringMode.Buffered;
            this.customerIDSpinEdit.Properties.NullText = "";
            this.customerIDSpinEdit.Properties.PopupSizeable = false;
            this.customerIDSpinEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.customerIDSpinEdit.Properties.ValueMember = "Customer_LocID";
            this.customerIDSpinEdit.Size = new System.Drawing.Size(479, 20);
            this.customerIDSpinEdit.StyleController = this.dataLayoutControl1;
            this.customerIDSpinEdit.TabIndex = 4;
            this.customerIDSpinEdit.Validating += new System.ComponentModel.CancelEventHandler(this.customerIDSpinEdit_Validating);
            // 
            // vwCustomersBindingSource
            // 
            this.vwCustomersBindingSource.DataMember = "vw_Customers";
            this.vwCustomersBindingSource.DataSource = this.dsCustomers;
            // 
            // dsCustomers
            // 
            this.dsCustomers.DataSetName = "dsCustomers";
            this.dsCustomers.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // dsFNOLBindingSource
            // 
            this.dsFNOLBindingSource.DataSource = this.dsFNOL;
            this.dsFNOLBindingSource.Position = 0;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup2,
            this.layoutControlItem2,
            this.layoutControlItem5,
            this.layoutControlItem3,
            this.layoutControlItem6,
            this.layoutControlItem4,
            this.layoutControlItem7,
            this.layoutControlItem8,
            this.layoutControlItem9,
            this.layoutControlItem10,
            this.layoutControlItem11,
            this.layoutControlItem12});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(612, 281);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.AllowDrawBackground = false;
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "autoGeneratedGroup0";
            this.layoutControlGroup2.Size = new System.Drawing.Size(592, 3);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.registrationTextEdit;
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 3);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(592, 24);
            this.layoutControlItem2.Text = "Vehicle Registration:";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(106, 13);
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.driverPhoneNumberSpinEdit;
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 75);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(592, 24);
            this.layoutControlItem5.Text = "Driver Phone Number:";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(106, 13);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.driverNameTextEdit;
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 51);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(592, 24);
            this.layoutControlItem3.Text = "Driver Name:";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(106, 13);
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.customerIDSpinEdit;
            this.layoutControlItem6.Location = new System.Drawing.Point(0, 27);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(592, 24);
            this.layoutControlItem6.Text = "Customer Name:";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(106, 13);
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.damageDescriptionTextEdit;
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 99);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(592, 24);
            this.layoutControlItem4.Text = "Damage Description:";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(106, 13);
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.isDriveableCheckEdit;
            this.layoutControlItem7.Location = new System.Drawing.Point(0, 123);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(592, 23);
            this.layoutControlItem7.Text = "is Driveable:";
            this.layoutControlItem7.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem7.TextVisible = false;
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.isFireDamageCheckEdit;
            this.layoutControlItem8.Location = new System.Drawing.Point(0, 146);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(592, 23);
            this.layoutControlItem8.Text = "is Fire Damage:";
            this.layoutControlItem8.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem8.TextVisible = false;
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.Control = this.isWheelDamageCheckEdit;
            this.layoutControlItem9.Location = new System.Drawing.Point(0, 169);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(592, 23);
            this.layoutControlItem9.Text = "is Wheel Damage:";
            this.layoutControlItem9.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem9.TextVisible = false;
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.Control = this.isEngineDamageCheckEdit;
            this.layoutControlItem10.Location = new System.Drawing.Point(0, 192);
            this.layoutControlItem10.Name = "layoutControlItem10";
            this.layoutControlItem10.Size = new System.Drawing.Size(592, 23);
            this.layoutControlItem10.Text = "is Engine Damage:";
            this.layoutControlItem10.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem10.TextVisible = false;
            // 
            // layoutControlItem11
            // 
            this.layoutControlItem11.Control = this.isBodyDamageCheckEdit;
            this.layoutControlItem11.Location = new System.Drawing.Point(0, 215);
            this.layoutControlItem11.Name = "layoutControlItem11";
            this.layoutControlItem11.Size = new System.Drawing.Size(592, 23);
            this.layoutControlItem11.Text = "is Body Damage:";
            this.layoutControlItem11.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem11.TextVisible = false;
            // 
            // layoutControlItem12
            // 
            this.layoutControlItem12.Control = this.is3rdPartyInvolvedCheckEdit;
            this.layoutControlItem12.Location = new System.Drawing.Point(0, 238);
            this.layoutControlItem12.Name = "layoutControlItem12";
            this.layoutControlItem12.Size = new System.Drawing.Size(592, 23);
            this.layoutControlItem12.Text = "is3rd Party Involved:";
            this.layoutControlItem12.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem12.TextVisible = false;
            // 
            // completionWizardPage1
            // 
            this.completionWizardPage1.Margin = new System.Windows.Forms.Padding(4);
            this.completionWizardPage1.Name = "completionWizardPage1";
            this.completionWizardPage1.Size = new System.Drawing.Size(1062, 378);
            this.completionWizardPage1.Click += new System.EventHandler(this.completionWizardPage1_Click);
            // 
            // wizardPage2
            // 
            this.wizardPage2.Controls.Add(this.layoutControl1);
            this.wizardPage2.Name = "wizardPage2";
            this.wizardPage2.Size = new System.Drawing.Size(1062, 378);
            this.wizardPage2.Text = "Incident";
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.otherNotesTextEdit);
            this.layoutControl1.Controls.Add(this.isDriverAtFaultCheckEdit);
            this.layoutControl1.Controls.Add(this.locationTextEdit);
            this.layoutControl1.Controls.Add(this.incidentDetailsTextEdit);
            this.layoutControl1.Controls.Add(this.dateOfIncidentDateEdit);
            this.layoutControl1.Controls.Add(this.eventNameTextEdit);
            this.layoutControl1.Controls.Add(this.timeofIncidentTimeEdit);
            this.layoutControl1.Location = new System.Drawing.Point(3, 3);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(947, 249, 450, 400);
            this.layoutControl1.Root = this.layoutControlGroup3;
            this.layoutControl1.Size = new System.Drawing.Size(539, 295);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // otherNotesTextEdit
            // 
            this.otherNotesTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.fNOLBindingSource, "otherNotes", true));
            this.otherNotesTextEdit.Location = new System.Drawing.Point(100, 155);
            this.otherNotesTextEdit.Name = "otherNotesTextEdit";
            this.otherNotesTextEdit.Size = new System.Drawing.Size(427, 20);
            this.otherNotesTextEdit.StyleController = this.layoutControl1;
            this.otherNotesTextEdit.TabIndex = 11;
            // 
            // isDriverAtFaultCheckEdit
            // 
            this.isDriverAtFaultCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.fNOLBindingSource, "isDriverAtFault", true));
            this.isDriverAtFaultCheckEdit.Location = new System.Drawing.Point(12, 132);
            this.isDriverAtFaultCheckEdit.Name = "isDriverAtFaultCheckEdit";
            this.isDriverAtFaultCheckEdit.Properties.Caption = "Is driver at fault?";
            this.isDriverAtFaultCheckEdit.Size = new System.Drawing.Size(515, 19);
            this.isDriverAtFaultCheckEdit.StyleController = this.layoutControl1;
            this.isDriverAtFaultCheckEdit.TabIndex = 10;
            // 
            // locationTextEdit
            // 
            this.locationTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.fNOLBindingSource, "location", true));
            this.locationTextEdit.Location = new System.Drawing.Point(100, 84);
            this.locationTextEdit.Name = "locationTextEdit";
            this.locationTextEdit.Size = new System.Drawing.Size(427, 20);
            this.locationTextEdit.StyleController = this.layoutControl1;
            this.locationTextEdit.TabIndex = 8;
            // 
            // incidentDetailsTextEdit
            // 
            this.incidentDetailsTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.fNOLBindingSource, "incidentDetails", true));
            this.incidentDetailsTextEdit.Location = new System.Drawing.Point(100, 60);
            this.incidentDetailsTextEdit.Name = "incidentDetailsTextEdit";
            this.incidentDetailsTextEdit.Size = new System.Drawing.Size(427, 20);
            this.incidentDetailsTextEdit.StyleController = this.layoutControl1;
            this.incidentDetailsTextEdit.TabIndex = 7;
            // 
            // dateOfIncidentDateEdit
            // 
            this.dateOfIncidentDateEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.fNOLBindingSource, "dateOfIncident", true));
            this.dateOfIncidentDateEdit.EditValue = null;
            this.dateOfIncidentDateEdit.Location = new System.Drawing.Point(100, 12);
            this.dateOfIncidentDateEdit.Name = "dateOfIncidentDateEdit";
            this.dateOfIncidentDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateOfIncidentDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateOfIncidentDateEdit.Size = new System.Drawing.Size(427, 20);
            this.dateOfIncidentDateEdit.StyleController = this.layoutControl1;
            this.dateOfIncidentDateEdit.TabIndex = 5;
            // 
            // eventNameTextEdit
            // 
            this.eventNameTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.fNOLBindingSource, "typeOfIncident", true));
            this.eventNameTextEdit.Location = new System.Drawing.Point(100, 36);
            this.eventNameTextEdit.Name = "eventNameTextEdit";
            this.eventNameTextEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.eventNameTextEdit.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("eventName", "event Name", 68, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.eventNameTextEdit.Properties.DataSource = this.vwEventTypesBindingSource1;
            this.eventNameTextEdit.Properties.DisplayMember = "eventName";
            this.eventNameTextEdit.Properties.NullText = "";
            this.eventNameTextEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.eventNameTextEdit.Properties.ValueMember = "id";
            this.eventNameTextEdit.Size = new System.Drawing.Size(427, 20);
            this.eventNameTextEdit.StyleController = this.layoutControl1;
            this.eventNameTextEdit.TabIndex = 14;
            // 
            // vwEventTypesBindingSource1
            // 
            this.vwEventTypesBindingSource1.DataMember = "vwEventTypes";
            this.vwEventTypesBindingSource1.DataSource = this.dsEventTypesBindingSource;
            // 
            // dsEventTypesBindingSource
            // 
            this.dsEventTypesBindingSource.DataSource = this.dsEventTypes;
            this.dsEventTypesBindingSource.Position = 0;
            // 
            // dsEventTypes
            // 
            this.dsEventTypes.DataSetName = "dsEventTypes";
            this.dsEventTypes.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // timeofIncidentTimeEdit
            // 
            this.timeofIncidentTimeEdit.CausesValidation = false;
            this.timeofIncidentTimeEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.fNOLBindingSource, "timeofIncident", true));
            this.timeofIncidentTimeEdit.EditValue = new System.DateTime(2017, 10, 16, 0, 0, 0, 0);
            this.timeofIncidentTimeEdit.Location = new System.Drawing.Point(100, 108);
            this.timeofIncidentTimeEdit.Name = "timeofIncidentTimeEdit";
            this.timeofIncidentTimeEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.timeofIncidentTimeEdit.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.timeofIncidentTimeEdit.Properties.Mask.EditMask = "t";
            this.timeofIncidentTimeEdit.Size = new System.Drawing.Size(427, 20);
            this.timeofIncidentTimeEdit.StyleController = this.layoutControl1;
            this.timeofIncidentTimeEdit.TabIndex = 9;
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup3.GroupBordersVisible = false;
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem13,
            this.layoutControlItem15,
            this.layoutControlItem16,
            this.layoutControlItem17,
            this.layoutControlItem18,
            this.layoutControlItem19,
            this.emptySpaceItem1,
            this.layoutControlItem21});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup3.Name = "Root";
            this.layoutControlGroup3.Size = new System.Drawing.Size(539, 295);
            this.layoutControlGroup3.TextVisible = false;
            // 
            // layoutControlItem13
            // 
            this.layoutControlItem13.Control = this.dateOfIncidentDateEdit;
            this.layoutControlItem13.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem13.Name = "layoutControlItem13";
            this.layoutControlItem13.Size = new System.Drawing.Size(519, 24);
            this.layoutControlItem13.Text = "Date Of Incident:";
            this.layoutControlItem13.TextSize = new System.Drawing.Size(85, 13);
            // 
            // layoutControlItem15
            // 
            this.layoutControlItem15.Control = this.incidentDetailsTextEdit;
            this.layoutControlItem15.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItem15.Name = "layoutControlItem15";
            this.layoutControlItem15.Size = new System.Drawing.Size(519, 24);
            this.layoutControlItem15.Text = "Incident Details:";
            this.layoutControlItem15.TextSize = new System.Drawing.Size(85, 13);
            // 
            // layoutControlItem16
            // 
            this.layoutControlItem16.Control = this.locationTextEdit;
            this.layoutControlItem16.Location = new System.Drawing.Point(0, 72);
            this.layoutControlItem16.Name = "layoutControlItem16";
            this.layoutControlItem16.Size = new System.Drawing.Size(519, 24);
            this.layoutControlItem16.Text = "Location:";
            this.layoutControlItem16.TextSize = new System.Drawing.Size(85, 13);
            // 
            // layoutControlItem17
            // 
            this.layoutControlItem17.Control = this.timeofIncidentTimeEdit;
            this.layoutControlItem17.Location = new System.Drawing.Point(0, 96);
            this.layoutControlItem17.Name = "layoutControlItem17";
            this.layoutControlItem17.Size = new System.Drawing.Size(519, 24);
            this.layoutControlItem17.Text = "Time of Incident:";
            this.layoutControlItem17.TextSize = new System.Drawing.Size(85, 13);
            // 
            // layoutControlItem18
            // 
            this.layoutControlItem18.Control = this.isDriverAtFaultCheckEdit;
            this.layoutControlItem18.Location = new System.Drawing.Point(0, 120);
            this.layoutControlItem18.Name = "layoutControlItem18";
            this.layoutControlItem18.Size = new System.Drawing.Size(519, 23);
            this.layoutControlItem18.Text = "is Driver At Fault:";
            this.layoutControlItem18.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem18.TextVisible = false;
            // 
            // layoutControlItem19
            // 
            this.layoutControlItem19.Control = this.otherNotesTextEdit;
            this.layoutControlItem19.Location = new System.Drawing.Point(0, 143);
            this.layoutControlItem19.Name = "layoutControlItem19";
            this.layoutControlItem19.Size = new System.Drawing.Size(519, 24);
            this.layoutControlItem19.Text = "Other Notes:";
            this.layoutControlItem19.TextSize = new System.Drawing.Size(85, 13);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 167);
            this.emptySpaceItem1.MinSize = new System.Drawing.Size(104, 24);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(519, 108);
            this.emptySpaceItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem21
            // 
            this.layoutControlItem21.Control = this.eventNameTextEdit;
            this.layoutControlItem21.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem21.Name = "layoutControlItem21";
            this.layoutControlItem21.Size = new System.Drawing.Size(519, 24);
            this.layoutControlItem21.Text = "Type Of Incident:";
            this.layoutControlItem21.TextSize = new System.Drawing.Size(85, 13);
            // 
            // wizardPage3
            // 
            this.wizardPage3.Controls.Add(this.layoutControl2);
            this.wizardPage3.Name = "wizardPage3";
            this.wizardPage3.Size = new System.Drawing.Size(1062, 378);
            this.wizardPage3.Text = "Accident";
            // 
            // layoutControl2
            // 
            this.layoutControl2.Controls.Add(this.no3rdPartyVehiclesSpinEdit);
            this.layoutControl2.Controls.Add(this.supportingPhotosCheckEdit);
            this.layoutControl2.Controls.Add(this.speedSpinEdit);
            this.layoutControl2.Controls.Add(this.weatherConditionsSpinEdit);
            this.layoutControl2.Controls.Add(this.roadConditionsSpinEdit);
            this.layoutControl2.Location = new System.Drawing.Point(3, 3);
            this.layoutControl2.Name = "layoutControl2";
            this.layoutControl2.Root = this.layoutControlGroup4;
            this.layoutControl2.Size = new System.Drawing.Size(395, 372);
            this.layoutControl2.TabIndex = 0;
            this.layoutControl2.Text = "layoutControl2";
            // 
            // no3rdPartyVehiclesSpinEdit
            // 
            this.no3rdPartyVehiclesSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.fNOLBindingSource, "no3rdPartyVehicles", true));
            this.no3rdPartyVehiclesSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.no3rdPartyVehiclesSpinEdit.Location = new System.Drawing.Point(140, 107);
            this.no3rdPartyVehiclesSpinEdit.Name = "no3rdPartyVehiclesSpinEdit";
            this.no3rdPartyVehiclesSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.no3rdPartyVehiclesSpinEdit.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.no3rdPartyVehiclesSpinEdit.Size = new System.Drawing.Size(243, 20);
            this.no3rdPartyVehiclesSpinEdit.StyleController = this.layoutControl2;
            this.no3rdPartyVehiclesSpinEdit.TabIndex = 9;
            // 
            // supportingPhotosCheckEdit
            // 
            this.supportingPhotosCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.fNOLBindingSource, "supportingPhotos", true));
            this.supportingPhotosCheckEdit.Location = new System.Drawing.Point(12, 84);
            this.supportingPhotosCheckEdit.Name = "supportingPhotosCheckEdit";
            this.supportingPhotosCheckEdit.Properties.Caption = "Supporting Photos?";
            this.supportingPhotosCheckEdit.Size = new System.Drawing.Size(371, 19);
            this.supportingPhotosCheckEdit.StyleController = this.layoutControl2;
            this.supportingPhotosCheckEdit.TabIndex = 8;
            // 
            // speedSpinEdit
            // 
            this.speedSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.fNOLBindingSource, "speed", true));
            this.speedSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.speedSpinEdit.Location = new System.Drawing.Point(140, 12);
            this.speedSpinEdit.Name = "speedSpinEdit";
            this.speedSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.speedSpinEdit.Size = new System.Drawing.Size(243, 20);
            this.speedSpinEdit.StyleController = this.layoutControl2;
            this.speedSpinEdit.TabIndex = 5;
            // 
            // weatherConditionsSpinEdit
            // 
            this.weatherConditionsSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.fNOLBindingSource, "weatherConditions", true));
            this.weatherConditionsSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.weatherConditionsSpinEdit.Location = new System.Drawing.Point(140, 36);
            this.weatherConditionsSpinEdit.Name = "weatherConditionsSpinEdit";
            this.weatherConditionsSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.weatherConditionsSpinEdit.Properties.EditValueChangedFiringMode = DevExpress.XtraEditors.Controls.EditValueChangedFiringMode.Buffered;
            this.weatherConditionsSpinEdit.Properties.NullText = "";
            this.weatherConditionsSpinEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.weatherConditionsSpinEdit.Size = new System.Drawing.Size(243, 20);
            this.weatherConditionsSpinEdit.StyleController = this.layoutControl2;
            this.weatherConditionsSpinEdit.TabIndex = 6;
            // 
            // roadConditionsSpinEdit
            // 
            this.roadConditionsSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.fNOLBindingSource, "roadConditions", true));
            this.roadConditionsSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.roadConditionsSpinEdit.Location = new System.Drawing.Point(140, 60);
            this.roadConditionsSpinEdit.Name = "roadConditionsSpinEdit";
            this.roadConditionsSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.roadConditionsSpinEdit.Properties.EditValueChangedFiringMode = DevExpress.XtraEditors.Controls.EditValueChangedFiringMode.Buffered;
            this.roadConditionsSpinEdit.Properties.NullText = "";
            this.roadConditionsSpinEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.roadConditionsSpinEdit.Size = new System.Drawing.Size(243, 20);
            this.roadConditionsSpinEdit.StyleController = this.layoutControl2;
            this.roadConditionsSpinEdit.TabIndex = 7;
            // 
            // layoutControlGroup4
            // 
            this.layoutControlGroup4.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup4.GroupBordersVisible = false;
            this.layoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem14,
            this.layoutControlItem20,
            this.layoutControlItem22,
            this.layoutControlItem23,
            this.layoutControlItem24});
            this.layoutControlGroup4.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup4.Name = "layoutControlGroup4";
            this.layoutControlGroup4.Size = new System.Drawing.Size(395, 372);
            this.layoutControlGroup4.TextVisible = false;
            // 
            // layoutControlItem14
            // 
            this.layoutControlItem14.Control = this.speedSpinEdit;
            this.layoutControlItem14.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem14.Name = "layoutControlItem14";
            this.layoutControlItem14.Size = new System.Drawing.Size(375, 24);
            this.layoutControlItem14.Text = "Vehicle Speed (mph):";
            this.layoutControlItem14.TextSize = new System.Drawing.Size(125, 13);
            // 
            // layoutControlItem20
            // 
            this.layoutControlItem20.Control = this.weatherConditionsSpinEdit;
            this.layoutControlItem20.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem20.Name = "layoutControlItem20";
            this.layoutControlItem20.Size = new System.Drawing.Size(375, 24);
            this.layoutControlItem20.Text = "Weather Conditions:";
            this.layoutControlItem20.TextSize = new System.Drawing.Size(125, 13);
            // 
            // layoutControlItem22
            // 
            this.layoutControlItem22.Control = this.roadConditionsSpinEdit;
            this.layoutControlItem22.CustomizationFormText = "Road Conditions:";
            this.layoutControlItem22.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItem22.Name = "layoutControlItem22";
            this.layoutControlItem22.Size = new System.Drawing.Size(375, 24);
            this.layoutControlItem22.Text = "Road Conditions:";
            this.layoutControlItem22.TextSize = new System.Drawing.Size(125, 13);
            // 
            // layoutControlItem23
            // 
            this.layoutControlItem23.Control = this.supportingPhotosCheckEdit;
            this.layoutControlItem23.Location = new System.Drawing.Point(0, 72);
            this.layoutControlItem23.Name = "layoutControlItem23";
            this.layoutControlItem23.Size = new System.Drawing.Size(375, 23);
            this.layoutControlItem23.Text = "supporting Photos:";
            this.layoutControlItem23.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem23.TextVisible = false;
            // 
            // layoutControlItem24
            // 
            this.layoutControlItem24.Control = this.no3rdPartyVehiclesSpinEdit;
            this.layoutControlItem24.Location = new System.Drawing.Point(0, 95);
            this.layoutControlItem24.Name = "layoutControlItem24";
            this.layoutControlItem24.Size = new System.Drawing.Size(375, 257);
            this.layoutControlItem24.Text = "No. Of 3rd Party Vehicles:";
            this.layoutControlItem24.TextSize = new System.Drawing.Size(125, 13);
            // 
            // vwEventTypesBindingSource
            // 
            this.vwEventTypesBindingSource.DataMember = "vwEventTypes";
            this.vwEventTypesBindingSource.DataSource = this.dsEventTypes;
            // 
            // vwRegistrationsBindingSource
            // 
            this.vwRegistrationsBindingSource.DataMember = "vwRegistrations";
            this.vwRegistrationsBindingSource.DataSource = this.dsRegistrationsBindingSource1;
            // 
            // dsRegistrationsBindingSource1
            // 
            this.dsRegistrationsBindingSource1.DataSource = this.dsRegistrations;
            this.dsRegistrationsBindingSource1.Position = 0;
            // 
            // fNOLTableAdapter
            // 
            this.fNOLTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.FNOLTableAdapter = this.fNOLTableAdapter;
            this.tableAdapterManager.UpdateOrder = SMART.dsFNOLTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // dsRegistrationsBindingSource
            // 
            this.dsRegistrationsBindingSource.DataSource = this.dsRegistrations;
            this.dsRegistrationsBindingSource.Position = 0;
            // 
            // vwRegistrationsTableAdapter
            // 
            this.vwRegistrationsTableAdapter.ClearBeforeFill = true;
            // 
            // vw_CustomersTableAdapter
            // 
            this.vw_CustomersTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager1
            // 
            this.tableAdapterManager1.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager1.Connection = null;
            this.tableAdapterManager1.UpdateOrder = SMART.dsRegistrationsTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // vwEventTypesTableAdapter
            // 
            this.vwEventTypesTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager2
            // 
            this.tableAdapterManager2.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager2.Connection = null;
            this.tableAdapterManager2.UpdateOrder = SMART.dsEventTypesTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // wizardPage4
            // 
            this.wizardPage4.Controls.Add(this.layoutControl3);
            this.wizardPage4.Name = "wizardPage4";
            this.wizardPage4.Size = new System.Drawing.Size(1062, 378);
            this.wizardPage4.Text = "Injury Details";
            // 
            // layoutControl3
            // 
            this.layoutControl3.Controls.Add(this.hospitalDetailsTextEdit);
            this.layoutControl3.Controls.Add(this.isHospitalNeededCheckEdit);
            this.layoutControl3.Controls.Add(this.treatmentReceivedCheckEdit);
            this.layoutControl3.Controls.Add(this.driverInjuryDetailsTextEdit);
            this.layoutControl3.Controls.Add(this.isDriverInjuredCheckEdit);
            this.layoutControl3.Location = new System.Drawing.Point(3, 3);
            this.layoutControl3.Name = "layoutControl3";
            this.layoutControl3.Root = this.layoutControlGroup5;
            this.layoutControl3.Size = new System.Drawing.Size(455, 368);
            this.layoutControl3.TabIndex = 0;
            this.layoutControl3.Text = "layoutControl3";
            // 
            // layoutControlGroup5
            // 
            this.layoutControlGroup5.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup5.GroupBordersVisible = false;
            this.layoutControlGroup5.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem25,
            this.layoutControlItem26,
            this.layoutControlItem27,
            this.layoutControlItem28,
            this.layoutControlItem29});
            this.layoutControlGroup5.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup5.Name = "layoutControlGroup5";
            this.layoutControlGroup5.Size = new System.Drawing.Size(455, 368);
            this.layoutControlGroup5.TextVisible = false;
            // 
            // isDriverInjuredCheckEdit
            // 
            this.isDriverInjuredCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.fNOLBindingSource, "isDriverInjured", true));
            this.isDriverInjuredCheckEdit.Location = new System.Drawing.Point(12, 12);
            this.isDriverInjuredCheckEdit.Name = "isDriverInjuredCheckEdit";
            this.isDriverInjuredCheckEdit.Properties.Caption = "Is Driver Injured?";
            this.isDriverInjuredCheckEdit.Size = new System.Drawing.Size(431, 19);
            this.isDriverInjuredCheckEdit.StyleController = this.layoutControl3;
            this.isDriverInjuredCheckEdit.TabIndex = 5;
            this.isDriverInjuredCheckEdit.CheckedChanged += new System.EventHandler(this.isDriverInjuredCheckEdit_CheckedChanged);
            // 
            // layoutControlItem25
            // 
            this.layoutControlItem25.Control = this.isDriverInjuredCheckEdit;
            this.layoutControlItem25.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem25.Name = "layoutControlItem25";
            this.layoutControlItem25.Size = new System.Drawing.Size(435, 23);
            this.layoutControlItem25.Text = "is Driver Injured:";
            this.layoutControlItem25.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem25.TextVisible = false;
            // 
            // driverInjuryDetailsTextEdit
            // 
            this.driverInjuryDetailsTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.fNOLBindingSource, "driverInjuryDetails", true));
            this.driverInjuryDetailsTextEdit.Enabled = false;
            this.driverInjuryDetailsTextEdit.Location = new System.Drawing.Point(116, 35);
            this.driverInjuryDetailsTextEdit.Name = "driverInjuryDetailsTextEdit";
            this.driverInjuryDetailsTextEdit.Size = new System.Drawing.Size(327, 20);
            this.driverInjuryDetailsTextEdit.StyleController = this.layoutControl3;
            this.driverInjuryDetailsTextEdit.TabIndex = 6;
            // 
            // layoutControlItem26
            // 
            this.layoutControlItem26.Control = this.driverInjuryDetailsTextEdit;
            this.layoutControlItem26.Location = new System.Drawing.Point(0, 23);
            this.layoutControlItem26.Name = "layoutControlItem26";
            this.layoutControlItem26.Size = new System.Drawing.Size(435, 24);
            this.layoutControlItem26.Text = "Driver Injury Details:";
            this.layoutControlItem26.TextSize = new System.Drawing.Size(100, 13);
            // 
            // treatmentReceivedCheckEdit
            // 
            this.treatmentReceivedCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.fNOLBindingSource, "treatmentReceived", true));
            this.treatmentReceivedCheckEdit.Enabled = false;
            this.treatmentReceivedCheckEdit.Location = new System.Drawing.Point(12, 59);
            this.treatmentReceivedCheckEdit.Name = "treatmentReceivedCheckEdit";
            this.treatmentReceivedCheckEdit.Properties.Caption = "Has Driver Received Treatment?";
            this.treatmentReceivedCheckEdit.Size = new System.Drawing.Size(431, 19);
            this.treatmentReceivedCheckEdit.StyleController = this.layoutControl3;
            this.treatmentReceivedCheckEdit.TabIndex = 7;
            // 
            // layoutControlItem27
            // 
            this.layoutControlItem27.Control = this.treatmentReceivedCheckEdit;
            this.layoutControlItem27.Location = new System.Drawing.Point(0, 47);
            this.layoutControlItem27.Name = "layoutControlItem27";
            this.layoutControlItem27.Size = new System.Drawing.Size(435, 23);
            this.layoutControlItem27.Text = "treatment Received:";
            this.layoutControlItem27.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem27.TextVisible = false;
            // 
            // isHospitalNeededCheckEdit
            // 
            this.isHospitalNeededCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.fNOLBindingSource, "isHospitalNeeded", true));
            this.isHospitalNeededCheckEdit.Enabled = false;
            this.isHospitalNeededCheckEdit.Location = new System.Drawing.Point(12, 82);
            this.isHospitalNeededCheckEdit.Name = "isHospitalNeededCheckEdit";
            this.isHospitalNeededCheckEdit.Properties.Caption = "Is Hospital Needed?";
            this.isHospitalNeededCheckEdit.Size = new System.Drawing.Size(431, 19);
            this.isHospitalNeededCheckEdit.StyleController = this.layoutControl3;
            this.isHospitalNeededCheckEdit.TabIndex = 8;
            // 
            // layoutControlItem28
            // 
            this.layoutControlItem28.Control = this.isHospitalNeededCheckEdit;
            this.layoutControlItem28.Location = new System.Drawing.Point(0, 70);
            this.layoutControlItem28.Name = "layoutControlItem28";
            this.layoutControlItem28.Size = new System.Drawing.Size(435, 23);
            this.layoutControlItem28.Text = "is Hospital Needed:";
            this.layoutControlItem28.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem28.TextVisible = false;
            // 
            // hospitalDetailsTextEdit
            // 
            this.hospitalDetailsTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.fNOLBindingSource, "hospitalDetails", true));
            this.hospitalDetailsTextEdit.Enabled = false;
            this.hospitalDetailsTextEdit.Location = new System.Drawing.Point(116, 105);
            this.hospitalDetailsTextEdit.Name = "hospitalDetailsTextEdit";
            this.hospitalDetailsTextEdit.Size = new System.Drawing.Size(327, 20);
            this.hospitalDetailsTextEdit.StyleController = this.layoutControl3;
            this.hospitalDetailsTextEdit.TabIndex = 9;
            // 
            // layoutControlItem29
            // 
            this.layoutControlItem29.Control = this.hospitalDetailsTextEdit;
            this.layoutControlItem29.Location = new System.Drawing.Point(0, 93);
            this.layoutControlItem29.Name = "layoutControlItem29";
            this.layoutControlItem29.Size = new System.Drawing.Size(435, 255);
            this.layoutControlItem29.Text = "Hospital Details:";
            this.layoutControlItem29.TextSize = new System.Drawing.Size(100, 13);
            // 
            // wizardPage5
            // 
            this.wizardPage5.Controls.Add(this.layoutControl4);
            this.wizardPage5.Name = "wizardPage5";
            this.wizardPage5.Size = new System.Drawing.Size(1062, 378);
            this.wizardPage5.Text = "Third Party";
            // 
            // layoutControl4
            // 
            this.layoutControl4.Controls.Add(this._3rdPartyInsuranceTextEdit);
            this.layoutControl4.Controls.Add(this._3rdPartyModelTextEdit);
            this.layoutControl4.Controls.Add(this._3rdPartyMakeTextEdit);
            this.layoutControl4.Controls.Add(this._3rdPartyInjuryDetailsTextEdit);
            this.layoutControl4.Controls.Add(this._3rdPartyIsInjuredCheckEdit);
            this.layoutControl4.Controls.Add(this._3rdPartyDamageDescriptionTextEdit);
            this.layoutControl4.Controls.Add(this._3rdPartyPhoneSpinEdit);
            this.layoutControl4.Controls.Add(this._3rdPartynameTextEdit);
            this.layoutControl4.Controls.Add(this._3rdPartyRegTextEdit);
            this.layoutControl4.Location = new System.Drawing.Point(3, 3);
            this.layoutControl4.Name = "layoutControl4";
            this.layoutControl4.Root = this.layoutControlGroup6;
            this.layoutControl4.Size = new System.Drawing.Size(471, 371);
            this.layoutControl4.TabIndex = 0;
            this.layoutControl4.Text = "layoutControl4";
            // 
            // layoutControlGroup6
            // 
            this.layoutControlGroup6.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup6.GroupBordersVisible = false;
            this.layoutControlGroup6.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem30,
            this.layoutControlItem31,
            this.layoutControlItem32,
            this.layoutControlItem33,
            this.layoutControlItem34,
            this.layoutControlItem35,
            this.layoutControlItem36,
            this.layoutControlItem37,
            this.layoutControlItem38});
            this.layoutControlGroup6.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup6.Name = "layoutControlGroup6";
            this.layoutControlGroup6.Size = new System.Drawing.Size(471, 371);
            this.layoutControlGroup6.TextVisible = false;
            // 
            // _3rdPartyRegTextEdit
            // 
            this._3rdPartyRegTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.fNOLBindingSource, "3rdPartyReg", true));
            this._3rdPartyRegTextEdit.Location = new System.Drawing.Point(115, 12);
            this._3rdPartyRegTextEdit.Name = "_3rdPartyRegTextEdit";
            this._3rdPartyRegTextEdit.Size = new System.Drawing.Size(344, 20);
            this._3rdPartyRegTextEdit.StyleController = this.layoutControl4;
            this._3rdPartyRegTextEdit.TabIndex = 5;
            // 
            // layoutControlItem30
            // 
            this.layoutControlItem30.Control = this._3rdPartyRegTextEdit;
            this.layoutControlItem30.CustomizationFormText = "3rd Party Registration:";
            this.layoutControlItem30.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem30.Name = "layoutControlItem30";
            this.layoutControlItem30.Size = new System.Drawing.Size(451, 24);
            this.layoutControlItem30.Text = "Registration:";
            this.layoutControlItem30.TextSize = new System.Drawing.Size(99, 13);
            // 
            // _3rdPartynameTextEdit
            // 
            this._3rdPartynameTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.fNOLBindingSource, "3rdPartyname", true));
            this._3rdPartynameTextEdit.Location = new System.Drawing.Point(115, 36);
            this._3rdPartynameTextEdit.Name = "_3rdPartynameTextEdit";
            this._3rdPartynameTextEdit.Size = new System.Drawing.Size(344, 20);
            this._3rdPartynameTextEdit.StyleController = this.layoutControl4;
            this._3rdPartynameTextEdit.TabIndex = 6;
            // 
            // layoutControlItem31
            // 
            this.layoutControlItem31.Control = this._3rdPartynameTextEdit;
            this.layoutControlItem31.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem31.Name = "layoutControlItem31";
            this.layoutControlItem31.Size = new System.Drawing.Size(451, 24);
            this.layoutControlItem31.Text = "Name:";
            this.layoutControlItem31.TextSize = new System.Drawing.Size(99, 13);
            // 
            // _3rdPartyPhoneSpinEdit
            // 
            this._3rdPartyPhoneSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.fNOLBindingSource, "3rdPartyPhone", true));
            this._3rdPartyPhoneSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this._3rdPartyPhoneSpinEdit.Location = new System.Drawing.Point(115, 60);
            this._3rdPartyPhoneSpinEdit.Name = "_3rdPartyPhoneSpinEdit";
            this._3rdPartyPhoneSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this._3rdPartyPhoneSpinEdit.Size = new System.Drawing.Size(344, 20);
            this._3rdPartyPhoneSpinEdit.StyleController = this.layoutControl4;
            this._3rdPartyPhoneSpinEdit.TabIndex = 7;
            // 
            // layoutControlItem32
            // 
            this.layoutControlItem32.Control = this._3rdPartyPhoneSpinEdit;
            this.layoutControlItem32.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItem32.Name = "layoutControlItem32";
            this.layoutControlItem32.Size = new System.Drawing.Size(451, 24);
            this.layoutControlItem32.Text = "Phone Number:";
            this.layoutControlItem32.TextSize = new System.Drawing.Size(99, 13);
            // 
            // _3rdPartyDamageDescriptionTextEdit
            // 
            this._3rdPartyDamageDescriptionTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.fNOLBindingSource, "3rdPartyDamageDescription", true));
            this._3rdPartyDamageDescriptionTextEdit.Location = new System.Drawing.Point(115, 84);
            this._3rdPartyDamageDescriptionTextEdit.Name = "_3rdPartyDamageDescriptionTextEdit";
            this._3rdPartyDamageDescriptionTextEdit.Size = new System.Drawing.Size(344, 20);
            this._3rdPartyDamageDescriptionTextEdit.StyleController = this.layoutControl4;
            this._3rdPartyDamageDescriptionTextEdit.TabIndex = 8;
            // 
            // layoutControlItem33
            // 
            this.layoutControlItem33.Control = this._3rdPartyDamageDescriptionTextEdit;
            this.layoutControlItem33.Location = new System.Drawing.Point(0, 72);
            this.layoutControlItem33.Name = "layoutControlItem33";
            this.layoutControlItem33.Size = new System.Drawing.Size(451, 24);
            this.layoutControlItem33.Text = "Damage Description:";
            this.layoutControlItem33.TextSize = new System.Drawing.Size(99, 13);
            // 
            // _3rdPartyIsInjuredCheckEdit
            // 
            this._3rdPartyIsInjuredCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.fNOLBindingSource, "3rdPartyIsInjured", true));
            this._3rdPartyIsInjuredCheckEdit.Location = new System.Drawing.Point(12, 108);
            this._3rdPartyIsInjuredCheckEdit.Name = "_3rdPartyIsInjuredCheckEdit";
            this._3rdPartyIsInjuredCheckEdit.Properties.Caption = "Is Injured?";
            this._3rdPartyIsInjuredCheckEdit.Size = new System.Drawing.Size(447, 19);
            this._3rdPartyIsInjuredCheckEdit.StyleController = this.layoutControl4;
            this._3rdPartyIsInjuredCheckEdit.TabIndex = 9;
            // 
            // layoutControlItem34
            // 
            this.layoutControlItem34.Control = this._3rdPartyIsInjuredCheckEdit;
            this.layoutControlItem34.Location = new System.Drawing.Point(0, 96);
            this.layoutControlItem34.Name = "layoutControlItem34";
            this.layoutControlItem34.Size = new System.Drawing.Size(451, 23);
            this.layoutControlItem34.Text = "3rd Party Is Injured:";
            this.layoutControlItem34.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem34.TextVisible = false;
            // 
            // _3rdPartyInjuryDetailsTextEdit
            // 
            this._3rdPartyInjuryDetailsTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.fNOLBindingSource, "3rdPartyInjuryDetails", true));
            this._3rdPartyInjuryDetailsTextEdit.Location = new System.Drawing.Point(115, 131);
            this._3rdPartyInjuryDetailsTextEdit.Name = "_3rdPartyInjuryDetailsTextEdit";
            this._3rdPartyInjuryDetailsTextEdit.Size = new System.Drawing.Size(344, 20);
            this._3rdPartyInjuryDetailsTextEdit.StyleController = this.layoutControl4;
            this._3rdPartyInjuryDetailsTextEdit.TabIndex = 10;
            // 
            // layoutControlItem35
            // 
            this.layoutControlItem35.Control = this._3rdPartyInjuryDetailsTextEdit;
            this.layoutControlItem35.Location = new System.Drawing.Point(0, 119);
            this.layoutControlItem35.Name = "layoutControlItem35";
            this.layoutControlItem35.Size = new System.Drawing.Size(451, 24);
            this.layoutControlItem35.Text = "Injury Details:";
            this.layoutControlItem35.TextSize = new System.Drawing.Size(99, 13);
            // 
            // _3rdPartyMakeTextEdit
            // 
            this._3rdPartyMakeTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.fNOLBindingSource, "3rdPartyMake", true));
            this._3rdPartyMakeTextEdit.Location = new System.Drawing.Point(115, 155);
            this._3rdPartyMakeTextEdit.Name = "_3rdPartyMakeTextEdit";
            this._3rdPartyMakeTextEdit.Size = new System.Drawing.Size(344, 20);
            this._3rdPartyMakeTextEdit.StyleController = this.layoutControl4;
            this._3rdPartyMakeTextEdit.TabIndex = 11;
            // 
            // layoutControlItem36
            // 
            this.layoutControlItem36.Control = this._3rdPartyMakeTextEdit;
            this.layoutControlItem36.Location = new System.Drawing.Point(0, 143);
            this.layoutControlItem36.Name = "layoutControlItem36";
            this.layoutControlItem36.Size = new System.Drawing.Size(451, 24);
            this.layoutControlItem36.Text = "Vehicle Make:";
            this.layoutControlItem36.TextSize = new System.Drawing.Size(99, 13);
            // 
            // _3rdPartyModelTextEdit
            // 
            this._3rdPartyModelTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.fNOLBindingSource, "3rdPartyModel", true));
            this._3rdPartyModelTextEdit.Location = new System.Drawing.Point(115, 179);
            this._3rdPartyModelTextEdit.Name = "_3rdPartyModelTextEdit";
            this._3rdPartyModelTextEdit.Size = new System.Drawing.Size(344, 20);
            this._3rdPartyModelTextEdit.StyleController = this.layoutControl4;
            this._3rdPartyModelTextEdit.TabIndex = 12;
            // 
            // layoutControlItem37
            // 
            this.layoutControlItem37.Control = this._3rdPartyModelTextEdit;
            this.layoutControlItem37.Location = new System.Drawing.Point(0, 167);
            this.layoutControlItem37.Name = "layoutControlItem37";
            this.layoutControlItem37.Size = new System.Drawing.Size(451, 24);
            this.layoutControlItem37.Text = "Vehicle Model:";
            this.layoutControlItem37.TextSize = new System.Drawing.Size(99, 13);
            // 
            // _3rdPartyInsuranceTextEdit
            // 
            this._3rdPartyInsuranceTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.fNOLBindingSource, "3rdPartyInsurance", true));
            this._3rdPartyInsuranceTextEdit.Location = new System.Drawing.Point(115, 203);
            this._3rdPartyInsuranceTextEdit.Name = "_3rdPartyInsuranceTextEdit";
            this._3rdPartyInsuranceTextEdit.Size = new System.Drawing.Size(344, 20);
            this._3rdPartyInsuranceTextEdit.StyleController = this.layoutControl4;
            this._3rdPartyInsuranceTextEdit.TabIndex = 13;
            // 
            // layoutControlItem38
            // 
            this.layoutControlItem38.Control = this._3rdPartyInsuranceTextEdit;
            this.layoutControlItem38.Location = new System.Drawing.Point(0, 191);
            this.layoutControlItem38.Name = "layoutControlItem38";
            this.layoutControlItem38.Size = new System.Drawing.Size(451, 160);
            this.layoutControlItem38.Text = "Insurance Details:";
            this.layoutControlItem38.TextSize = new System.Drawing.Size(99, 13);
            // 
            // frmNewFNOL
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1122, 546);
            this.Controls.Add(this.wizardControl1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "frmNewFNOL";
            this.Text = "frmNewFNOL";
            this.Load += new System.EventHandler(this.frmNewFNOL_Load);
            ((System.ComponentModel.ISupportInitialize)(this.wizardControl1)).EndInit();
            this.wizardControl1.ResumeLayout(false);
            this.wizardPage1.ResumeLayout(false);
            this.wizardPage1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.registrationFreeTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fNOLBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsFNOL)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).EndInit();
            this.dataLayoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.is3rdPartyInvolvedCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.isBodyDamageCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.isEngineDamageCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.isWheelDamageCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.isFireDamageCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.isDriveableCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.damageDescriptionTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.driverNameTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.registrationTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vwRegistrationsBindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsRegistrations)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.driverPhoneNumberSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.customerIDSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vwCustomersBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsCustomers)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsFNOLBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).EndInit();
            this.wizardPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.otherNotesTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.isDriverAtFaultCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.locationTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.incidentDetailsTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateOfIncidentDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateOfIncidentDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.eventNameTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vwEventTypesBindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsEventTypesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsEventTypes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.timeofIncidentTimeEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem21)).EndInit();
            this.wizardPage3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).EndInit();
            this.layoutControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.no3rdPartyVehiclesSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.supportingPhotosCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.speedSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.weatherConditionsSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.roadConditionsSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vwEventTypesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vwRegistrationsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsRegistrationsBindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsRegistrationsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.behaviorManager1)).EndInit();
            this.wizardPage4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl3)).EndInit();
            this.layoutControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.isDriverInjuredCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.driverInjuryDetailsTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.treatmentReceivedCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.isHospitalNeededCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.hospitalDetailsTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem29)).EndInit();
            this.wizardPage5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl4)).EndInit();
            this.layoutControl4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._3rdPartyRegTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._3rdPartynameTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem31)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._3rdPartyPhoneSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem32)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._3rdPartyDamageDescriptionTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem33)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._3rdPartyIsInjuredCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem34)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._3rdPartyInjuryDetailsTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem35)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._3rdPartyMakeTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem36)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._3rdPartyModelTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem37)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._3rdPartyInsuranceTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem38)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraWizard.WizardControl wizardControl1;
        private DevExpress.XtraWizard.WizardPage wizardPage1;
        private DevExpress.XtraWizard.CompletionWizardPage completionWizardPage1;
        private dsFNOL dsFNOL;
        private System.Windows.Forms.BindingSource fNOLBindingSource;
        private dsFNOLTableAdapters.FNOLTableAdapter fNOLTableAdapter;
        private dsFNOLTableAdapters.TableAdapterManager tableAdapterManager;
        private System.Windows.Forms.BindingSource dsRegistrationsBindingSource1;
        private dsRegistrations dsRegistrations;
        private System.Windows.Forms.BindingSource dsRegistrationsBindingSource;
        private System.Windows.Forms.BindingSource vwRegistrationsBindingSource;
        private dsRegistrationsTableAdapters.vwRegistrationsTableAdapter vwRegistrationsTableAdapter;
        private DevExpress.XtraDataLayout.DataLayoutControl dataLayoutControl1;
        private System.Windows.Forms.BindingSource dsFNOLBindingSource;
        private System.Windows.Forms.BindingSource vwRegistrationsBindingSource1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private dsCustomers dsCustomers;
        private System.Windows.Forms.BindingSource vwCustomersBindingSource;
        private dsCustomersTableAdapters.vw_CustomersTableAdapter vw_CustomersTableAdapter;
        private DevExpress.Utils.Behaviors.BehaviorManager behaviorManager1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraEditors.LookUpEdit registrationTextEdit;
        private DevExpress.XtraEditors.TextEdit driverPhoneNumberSpinEdit;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraEditors.LookUpEdit customerIDSpinEdit;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraEditors.TextEdit registrationFreeTextEdit;
        private System.Windows.Forms.CheckBox cbRegNotFound;
        private dsRegistrationsTableAdapters.TableAdapterManager tableAdapterManager1;
        private DevExpress.XtraEditors.TextEdit driverNameTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraEditors.TextEdit damageDescriptionTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraEditors.CheckEdit isWheelDamageCheckEdit;
        private DevExpress.XtraEditors.CheckEdit isFireDamageCheckEdit;
        private DevExpress.XtraEditors.CheckEdit isDriveableCheckEdit;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraEditors.CheckEdit isEngineDamageCheckEdit;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
        private DevExpress.XtraEditors.CheckEdit isBodyDamageCheckEdit;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem11;
        private DevExpress.XtraEditors.CheckEdit is3rdPartyInvolvedCheckEdit;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem12;
        private DevExpress.XtraWizard.WizardPage wizardPage2;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraEditors.TextEdit otherNotesTextEdit;
        private DevExpress.XtraEditors.CheckEdit isDriverAtFaultCheckEdit;
        private DevExpress.XtraEditors.TextEdit locationTextEdit;
        private DevExpress.XtraEditors.TextEdit incidentDetailsTextEdit;
        private DevExpress.XtraEditors.DateEdit dateOfIncidentDateEdit;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem13;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem15;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem16;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem17;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem18;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem19;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private dsEventTypes dsEventTypes;
        private System.Windows.Forms.BindingSource vwEventTypesBindingSource;
        private dsEventTypesTableAdapters.vwEventTypesTableAdapter vwEventTypesTableAdapter;
        private DevExpress.XtraEditors.LookUpEdit eventNameTextEdit;
        private System.Windows.Forms.BindingSource vwEventTypesBindingSource1;
        private System.Windows.Forms.BindingSource dsEventTypesBindingSource;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem21;
        private dsEventTypesTableAdapters.TableAdapterManager tableAdapterManager2;
        private DevExpress.XtraEditors.TimeEdit timeofIncidentTimeEdit;
        private DevExpress.XtraWizard.WizardPage wizardPage3;
        private DevExpress.XtraLayout.LayoutControl layoutControl2;
        private DevExpress.XtraEditors.SpinEdit no3rdPartyVehiclesSpinEdit;
        private DevExpress.XtraEditors.CheckEdit supportingPhotosCheckEdit;
        private DevExpress.XtraEditors.SpinEdit speedSpinEdit;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem14;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem20;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem22;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem23;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem24;
        private DevExpress.XtraEditors.LookUpEdit weatherConditionsSpinEdit;
        private DevExpress.XtraEditors.LookUpEdit roadConditionsSpinEdit;
        private DevExpress.XtraWizard.WizardPage wizardPage4;
        private DevExpress.XtraLayout.LayoutControl layoutControl3;
        private DevExpress.XtraEditors.TextEdit hospitalDetailsTextEdit;
        private DevExpress.XtraEditors.CheckEdit isHospitalNeededCheckEdit;
        private DevExpress.XtraEditors.CheckEdit treatmentReceivedCheckEdit;
        private DevExpress.XtraEditors.TextEdit driverInjuryDetailsTextEdit;
        private DevExpress.XtraEditors.CheckEdit isDriverInjuredCheckEdit;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem25;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem26;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem27;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem28;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem29;
        private DevExpress.XtraWizard.WizardPage wizardPage5;
        private DevExpress.XtraLayout.LayoutControl layoutControl4;
        private DevExpress.XtraEditors.TextEdit _3rdPartyInsuranceTextEdit;
        private DevExpress.XtraEditors.TextEdit _3rdPartyModelTextEdit;
        private DevExpress.XtraEditors.TextEdit _3rdPartyMakeTextEdit;
        private DevExpress.XtraEditors.TextEdit _3rdPartyInjuryDetailsTextEdit;
        private DevExpress.XtraEditors.CheckEdit _3rdPartyIsInjuredCheckEdit;
        private DevExpress.XtraEditors.TextEdit _3rdPartyDamageDescriptionTextEdit;
        private DevExpress.XtraEditors.SpinEdit _3rdPartyPhoneSpinEdit;
        private DevExpress.XtraEditors.TextEdit _3rdPartynameTextEdit;
        private DevExpress.XtraEditors.TextEdit _3rdPartyRegTextEdit;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem30;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem31;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem32;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem33;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem34;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem35;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem36;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem37;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem38;
    }
}