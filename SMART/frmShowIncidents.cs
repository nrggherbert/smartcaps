﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SMART
{
    public partial class frmShowIncidents : Form
    {
        public frmShowIncidents()
        {
            InitializeComponent();
        }

        private void fNOLBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.fNOLBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.dsFNOL);

        }

        private void frmShowIncidents_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'dsFNOLEvents.vw_FNOLEvents' table. You can move, or remove it, as needed.
            this.vw_FNOLEventsTableAdapter.Fill(this.dsFNOLEvents.vw_FNOLEvents);
            // TODO: This line of code loads data into the 'dsProcesses.FNOLProcesses' table. You can move, or remove it, as needed.
            this.fNOLProcessesTableAdapter.Fill(this.dsProcesses.FNOLProcesses);
            // TODO: This line of code loads data into the 'dsFNOLExternalCommunications.FNOLExternalCommunication' table. You can move, or remove it, as needed.
            this.fNOLExternalCommunicationTableAdapter.Fill(this.dsFNOLExternalCommunications.FNOLExternalCommunication);

            BindingSource bsFNOL = new BindingSource();
            BindingSource bsEvents = new BindingSource();
            BindingSource bsComms = new BindingSource();
            BindingSource bsProcesses = new BindingSource();
            BindingSource bsEstimate = new BindingSource();


            dgvFNOL.DataSource = bsFNOL;
            dgvEvents.DataSource = bsEvents;
            dgvComms.DataSource = bsComms;
            dgvProcesses.DataSource = bsProcesses;
            dgvEstimateDetails.DataSource = bsEstimate;

            SqlConnection con = new SqlConnection(Properties.Settings.Default.Properties["SMARTdevConnectionString"].DefaultValue.ToString());
            DataSet ds = new DataSet();

            SqlDataAdapter daFNOL = new SqlDataAdapter("SELECT * FROM FNOL", con);
            daFNOL.Fill(ds, "FNOL");

            SqlDataAdapter daEvents = new SqlDataAdapter("select * from vw_FNOLEvents", con);
            daEvents.Fill(ds, "fnolEvents");

            SqlDataAdapter daComms = new SqlDataAdapter("SELECT * FROM FNOLExternalCommunication", con);
            daComms.Fill(ds, "FNOLExternalCommunication");

            SqlDataAdapter daEstimate = new SqlDataAdapter("SELECT * FROM FNOLEstimateDetails", con);
            daEstimate.Fill(ds, "FNOLEstimateDetails");

            SqlDataAdapter daProcesses = new SqlDataAdapter("SELECT * FROM FNOLProcesses", con);
            daProcesses.Fill(ds, "FNOLProcesses");

            DataRelation dr = new DataRelation("FNOLEvents", ds.Tables["FNOL"].Columns["id"],
                                                    ds.Tables["fnolEvents"].Columns["fnolID"]);
            ds.Relations.Add(dr);

            DataRelation drComms = new DataRelation("FNOLComms", ds.Tables["FNOL"].Columns["id"],
                                                    ds.Tables["FNOLExternalCommunication"].Columns["fnolID"]);
            ds.Relations.Add(drComms);

            DataRelation drProcs = new DataRelation("FNOLProcesses", ds.Tables["FNOL"].Columns["id"],
                                                    ds.Tables["fnolProcesses"].Columns["fnolID"]);
            ds.Relations.Add(drProcs);

            DataRelation drEstimate = new DataRelation("FNOLEstimateDetails", ds.Tables["FNOL"].Columns["id"],
                                                    ds.Tables["FNOLEstimateDetails"].Columns["fnolID"]);
            ds.Relations.Add(drEstimate);

            bsFNOL.DataSource = ds;
            bsFNOL.DataMember = "FNOL";

            bsEvents.DataSource = bsFNOL;
            bsEvents.DataMember = "FNOLEvents";

            bsComms.DataSource = bsFNOL;
            bsComms.DataMember = "FNOLComms";

            bsProcesses.DataSource = bsFNOL;
            bsProcesses.DataMember = "FNOLProcesses";

            bsEstimate.DataSource = bsFNOL;
            bsEstimate.DataMember = "FNOLEstimateDetails";

/*
            dgvComms.AutoResizeColumns();
            dgvEvents.AutoResizeColumns();
            dgvProcesses.AutoResizeColumns();
*/


            // TODO: This line of code loads data into the 'dsFNOLEvents.fnolEvents' table. You can move, or remove it, as needed.
            /////////            this.fnolEventsTableAdapter.Fill(this.dsFNOLEvents.fnolEvents);
            // TODO: This line of code loads data into the 'dsFNOL.FNOL' table. You can move, or remove it, as needed.
            this.fNOLTableAdapter.Fill(this.dsFNOL.FNOL);



        }

        private void dgvProcesses_CellPainting(object sender, DataGridViewCellPaintingEventArgs e)
        {
        }

        private void dgvProcesses_CellPainting_1(object sender, DataGridViewCellPaintingEventArgs e)
        {
            if (e.RowIndex > -1)
            {
                if (e.ColumnIndex == 2)
                {
                    if (Convert.ToInt32(e.Value) == 100)
                    {
                        e.CellStyle.BackColor = Color.Green;
                    }
                    else if (Convert.ToInt32(e.Value) > 50)
                    {
                        e.CellStyle.BackColor = Color.Yellow;
                    }
                    else
                    {
                        e.CellStyle.BackColor = Color.Red;
                    }
                }
            }

        }
    }
}
