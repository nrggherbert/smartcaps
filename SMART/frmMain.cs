﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace SMART
{
    public partial class frmMain : Form
    {
        public frmMain()
        {
            InitializeComponent();
        }

        private void newToolStripMenuItem_Click(object sender, EventArgs e)
        {
                        frmNewFNOL newMDChild = new frmNewFNOL()
                        {
                            MdiParent = this,
                            Dock = DockStyle.Fill
                        };

            newMDChild.Show();
        }

        private void addressDetailGridControl_Click(object sender, EventArgs e)
        {

        }

        private void testToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            string sqlConnString = Properties.Settings.Default.Properties["SMARTdevConnectionString"].DefaultValue.ToString();
            SqlConnection sqlConn = new SqlConnection(sqlConnString);
            sqlConn.Open();


            SqlCommand cmdClearCAPSData = new SqlCommand();
            cmdClearCAPSData.Connection = sqlConn;
            cmdClearCAPSData.CommandType = CommandType.StoredProcedure;
            cmdClearCAPSData.CommandText = "clearCAPSData";
            cmdClearCAPSData.ExecuteNonQuery();
            sqlConn.Close();
            getXML();
            MessageBox.Show("Message Received");
        }

        private void viewOpenToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmShowIncidents newMDChild = new frmShowIncidents()
            {
                MdiParent = this,
                Dock = DockStyle.Fill
            };

            newMDChild.Show();

/*
            frmShowCAPS newMDChild = new frmShowCAPS()
            {
                MdiParent = this,
                Dock = DockStyle.Fill
            };

            newMDChild.Show();
*/

        }
    }
}
