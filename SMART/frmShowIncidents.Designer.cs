﻿namespace SMART
{
    partial class frmShowIncidents
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dsFNOL = new SMART.dsFNOL();
            this.fNOLBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.fNOLTableAdapter = new SMART.dsFNOLTableAdapters.FNOLTableAdapter();
            this.tableAdapterManager = new SMART.dsFNOLTableAdapters.TableAdapterManager();
            this.dgvFNOL = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn13 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dsFNOLExternalCommunications = new SMART.dsFNOLExternalCommunications();
            this.fNOLExternalCommunicationBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.fNOLExternalCommunicationTableAdapter = new SMART.dsFNOLExternalCommunicationsTableAdapters.FNOLExternalCommunicationTableAdapter();
            this.tableAdapterManager2 = new SMART.dsFNOLExternalCommunicationsTableAdapters.TableAdapterManager();
            this.dsProcesses = new SMART.dsProcesses();
            this.fNOLProcessesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.fNOLProcessesTableAdapter = new SMART.dsProcessesTableAdapters.FNOLProcessesTableAdapter();
            this.tableAdapterManager3 = new SMART.dsProcessesTableAdapters.TableAdapterManager();
            this.tableAdapterManager4 = new SMART.dsCustomersTableAdapters.TableAdapterManager();
            this.dsFNOLEvents = new SMART.dsFNOLEvents();
            this.vwFNOLEventsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.vw_FNOLEventsTableAdapter = new SMART.dsFNOLEventsTableAdapters.vw_FNOLEventsTableAdapter();
            this.dgvProcesses = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn20 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn21 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn22 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn23 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn24 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvComms = new System.Windows.Forms.DataGridView();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.dgvEvents = new System.Windows.Forms.DataGridView();
            this.eventDateTimeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.eventTypeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.dgvEstimateDetails = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn15 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn16 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn17 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn18 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dsFNOL)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fNOLBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvFNOL)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsFNOLExternalCommunications)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fNOLExternalCommunicationBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsProcesses)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fNOLProcessesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsFNOLEvents)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vwFNOLEventsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvProcesses)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvComms)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvEvents)).BeginInit();
            this.tabPage2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.tabPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvEstimateDetails)).BeginInit();
            this.SuspendLayout();
            // 
            // dsFNOL
            // 
            this.dsFNOL.DataSetName = "dsFNOL";
            this.dsFNOL.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // fNOLBindingSource
            // 
            this.fNOLBindingSource.DataMember = "FNOL";
            this.fNOLBindingSource.DataSource = this.dsFNOL;
            // 
            // fNOLTableAdapter
            // 
            this.fNOLTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.FNOLTableAdapter = this.fNOLTableAdapter;
            this.tableAdapterManager.UpdateOrder = SMART.dsFNOLTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // dgvFNOL
            // 
            this.dgvFNOL.AllowUserToAddRows = false;
            this.dgvFNOL.AllowUserToDeleteRows = false;
            this.dgvFNOL.AutoGenerateColumns = false;
            this.dgvFNOL.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvFNOL.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn5,
            this.dataGridViewTextBoxColumn6,
            this.dataGridViewTextBoxColumn7,
            this.dataGridViewTextBoxColumn8,
            this.dataGridViewTextBoxColumn10,
            this.dataGridViewTextBoxColumn11,
            this.dataGridViewTextBoxColumn12,
            this.dataGridViewTextBoxColumn13});
            this.dgvFNOL.DataSource = this.fNOLBindingSource;
            this.dgvFNOL.Dock = System.Windows.Forms.DockStyle.Top;
            this.dgvFNOL.Location = new System.Drawing.Point(0, 0);
            this.dgvFNOL.Name = "dgvFNOL";
            this.dgvFNOL.ReadOnly = true;
            this.dgvFNOL.Size = new System.Drawing.Size(1369, 220);
            this.dgvFNOL.TabIndex = 1;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "id";
            this.dataGridViewTextBoxColumn1.HeaderText = "id";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "registration";
            this.dataGridViewTextBoxColumn2.HeaderText = "registration";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.DataPropertyName = "driverName";
            this.dataGridViewTextBoxColumn5.HeaderText = "driverName";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.DataPropertyName = "driverPhoneNumber";
            this.dataGridViewTextBoxColumn6.HeaderText = "driverPhoneNumber";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.DataPropertyName = "damageDescription";
            this.dataGridViewTextBoxColumn7.HeaderText = "damageDescription";
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            this.dataGridViewTextBoxColumn7.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn8
            // 
            this.dataGridViewTextBoxColumn8.DataPropertyName = "dateOfIncident";
            this.dataGridViewTextBoxColumn8.HeaderText = "dateOfIncident";
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            this.dataGridViewTextBoxColumn8.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn10
            // 
            this.dataGridViewTextBoxColumn10.DataPropertyName = "incidentDetails";
            this.dataGridViewTextBoxColumn10.HeaderText = "incidentDetails";
            this.dataGridViewTextBoxColumn10.Name = "dataGridViewTextBoxColumn10";
            this.dataGridViewTextBoxColumn10.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn11
            // 
            this.dataGridViewTextBoxColumn11.DataPropertyName = "location";
            this.dataGridViewTextBoxColumn11.HeaderText = "location";
            this.dataGridViewTextBoxColumn11.Name = "dataGridViewTextBoxColumn11";
            this.dataGridViewTextBoxColumn11.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn12
            // 
            this.dataGridViewTextBoxColumn12.DataPropertyName = "timeofIncident";
            this.dataGridViewTextBoxColumn12.HeaderText = "timeofIncident";
            this.dataGridViewTextBoxColumn12.Name = "dataGridViewTextBoxColumn12";
            this.dataGridViewTextBoxColumn12.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn13
            // 
            this.dataGridViewTextBoxColumn13.DataPropertyName = "otherNotes";
            this.dataGridViewTextBoxColumn13.HeaderText = "otherNotes";
            this.dataGridViewTextBoxColumn13.Name = "dataGridViewTextBoxColumn13";
            this.dataGridViewTextBoxColumn13.ReadOnly = true;
            // 
            // dsFNOLExternalCommunications
            // 
            this.dsFNOLExternalCommunications.DataSetName = "dsFNOLExternalCommunications";
            this.dsFNOLExternalCommunications.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // fNOLExternalCommunicationBindingSource
            // 
            this.fNOLExternalCommunicationBindingSource.DataMember = "FNOLExternalCommunication";
            this.fNOLExternalCommunicationBindingSource.DataSource = this.dsFNOLExternalCommunications;
            // 
            // fNOLExternalCommunicationTableAdapter
            // 
            this.fNOLExternalCommunicationTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager2
            // 
            this.tableAdapterManager2.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager2.FNOLExternalCommunicationTableAdapter = this.fNOLExternalCommunicationTableAdapter;
            this.tableAdapterManager2.UpdateOrder = SMART.dsFNOLExternalCommunicationsTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // dsProcesses
            // 
            this.dsProcesses.DataSetName = "dsProcesses";
            this.dsProcesses.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // fNOLProcessesBindingSource
            // 
            this.fNOLProcessesBindingSource.DataMember = "FNOLProcesses";
            this.fNOLProcessesBindingSource.DataSource = this.dsProcesses;
            // 
            // fNOLProcessesTableAdapter
            // 
            this.fNOLProcessesTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager3
            // 
            this.tableAdapterManager3.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager3.FNOLProcessesTableAdapter = this.fNOLProcessesTableAdapter;
            this.tableAdapterManager3.UpdateOrder = SMART.dsProcessesTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // tableAdapterManager4
            // 
            this.tableAdapterManager4.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager4.Connection = null;
            this.tableAdapterManager4.UpdateOrder = SMART.dsCustomersTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // dsFNOLEvents
            // 
            this.dsFNOLEvents.DataSetName = "dsFNOLEvents";
            this.dsFNOLEvents.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // vwFNOLEventsBindingSource
            // 
            this.vwFNOLEventsBindingSource.DataMember = "vw_FNOLEvents";
            this.vwFNOLEventsBindingSource.DataSource = this.dsFNOLEvents;
            // 
            // vw_FNOLEventsTableAdapter
            // 
            this.vw_FNOLEventsTableAdapter.ClearBeforeFill = true;
            // 
            // dgvProcesses
            // 
            this.dgvProcesses.AllowUserToAddRows = false;
            this.dgvProcesses.AllowUserToDeleteRows = false;
            this.dgvProcesses.AutoGenerateColumns = false;
            this.dgvProcesses.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgvProcesses.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvProcesses.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn20,
            this.dataGridViewTextBoxColumn21,
            this.dataGridViewTextBoxColumn22,
            this.dataGridViewTextBoxColumn23,
            this.dataGridViewTextBoxColumn24});
            this.dgvProcesses.DataSource = this.fNOLProcessesBindingSource;
            this.dgvProcesses.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvProcesses.Location = new System.Drawing.Point(3, 3);
            this.dgvProcesses.Name = "dgvProcesses";
            this.dgvProcesses.ReadOnly = true;
            this.dgvProcesses.Size = new System.Drawing.Size(1355, 413);
            this.dgvProcesses.TabIndex = 9;
            this.dgvProcesses.CellPainting += new System.Windows.Forms.DataGridViewCellPaintingEventHandler(this.dgvProcesses_CellPainting_1);
            // 
            // dataGridViewTextBoxColumn20
            // 
            this.dataGridViewTextBoxColumn20.DataPropertyName = "workshopTask";
            this.dataGridViewTextBoxColumn20.HeaderText = "workshopTask";
            this.dataGridViewTextBoxColumn20.Name = "dataGridViewTextBoxColumn20";
            this.dataGridViewTextBoxColumn20.ReadOnly = true;
            this.dataGridViewTextBoxColumn20.Width = 102;
            // 
            // dataGridViewTextBoxColumn21
            // 
            this.dataGridViewTextBoxColumn21.DataPropertyName = "taskStatus";
            this.dataGridViewTextBoxColumn21.HeaderText = "taskStatus";
            this.dataGridViewTextBoxColumn21.Name = "dataGridViewTextBoxColumn21";
            this.dataGridViewTextBoxColumn21.ReadOnly = true;
            this.dataGridViewTextBoxColumn21.Width = 82;
            // 
            // dataGridViewTextBoxColumn22
            // 
            this.dataGridViewTextBoxColumn22.DataPropertyName = "progressPercentage";
            this.dataGridViewTextBoxColumn22.HeaderText = "progressPercentage";
            this.dataGridViewTextBoxColumn22.Name = "dataGridViewTextBoxColumn22";
            this.dataGridViewTextBoxColumn22.ReadOnly = true;
            this.dataGridViewTextBoxColumn22.Width = 127;
            // 
            // dataGridViewTextBoxColumn23
            // 
            this.dataGridViewTextBoxColumn23.DataPropertyName = "progressExpectedStartDate";
            this.dataGridViewTextBoxColumn23.HeaderText = "progressExpectedStartDate";
            this.dataGridViewTextBoxColumn23.Name = "dataGridViewTextBoxColumn23";
            this.dataGridViewTextBoxColumn23.ReadOnly = true;
            this.dataGridViewTextBoxColumn23.Width = 162;
            // 
            // dataGridViewTextBoxColumn24
            // 
            this.dataGridViewTextBoxColumn24.DataPropertyName = "progressCompletionDate";
            this.dataGridViewTextBoxColumn24.HeaderText = "progressCompletionDate";
            this.dataGridViewTextBoxColumn24.Name = "dataGridViewTextBoxColumn24";
            this.dataGridViewTextBoxColumn24.ReadOnly = true;
            this.dataGridViewTextBoxColumn24.Width = 147;
            // 
            // dgvComms
            // 
            this.dgvComms.AllowUserToAddRows = false;
            this.dgvComms.AllowUserToDeleteRows = false;
            this.dgvComms.AutoGenerateColumns = false;
            this.dgvComms.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvComms.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgvComms.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvComms.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn15,
            this.dataGridViewTextBoxColumn16,
            this.dataGridViewTextBoxColumn17,
            this.dataGridViewTextBoxColumn18});
            this.dgvComms.DataSource = this.fNOLExternalCommunicationBindingSource;
            this.dgvComms.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvComms.Location = new System.Drawing.Point(3, 3);
            this.dgvComms.Name = "dgvComms";
            this.dgvComms.ReadOnly = true;
            this.dgvComms.Size = new System.Drawing.Size(1355, 413);
            this.dgvComms.TabIndex = 10;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 220);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1369, 445);
            this.tabControl1.TabIndex = 11;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.dgvEvents);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1361, 419);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Events";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // dgvEvents
            // 
            this.dgvEvents.AllowUserToAddRows = false;
            this.dgvEvents.AllowUserToDeleteRows = false;
            this.dgvEvents.AutoGenerateColumns = false;
            this.dgvEvents.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvEvents.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvEvents.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.eventDateTimeDataGridViewTextBoxColumn,
            this.eventTypeDataGridViewTextBoxColumn});
            this.dgvEvents.DataSource = this.vwFNOLEventsBindingSource;
            this.dgvEvents.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvEvents.Location = new System.Drawing.Point(3, 3);
            this.dgvEvents.Name = "dgvEvents";
            this.dgvEvents.ReadOnly = true;
            this.dgvEvents.Size = new System.Drawing.Size(1355, 413);
            this.dgvEvents.TabIndex = 9;
            // 
            // eventDateTimeDataGridViewTextBoxColumn
            // 
            this.eventDateTimeDataGridViewTextBoxColumn.DataPropertyName = "EventDateTime";
            this.eventDateTimeDataGridViewTextBoxColumn.HeaderText = "EventDateTime";
            this.eventDateTimeDataGridViewTextBoxColumn.Name = "eventDateTimeDataGridViewTextBoxColumn";
            this.eventDateTimeDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // eventTypeDataGridViewTextBoxColumn
            // 
            this.eventTypeDataGridViewTextBoxColumn.DataPropertyName = "EventType";
            this.eventTypeDataGridViewTextBoxColumn.HeaderText = "EventType";
            this.eventTypeDataGridViewTextBoxColumn.Name = "eventTypeDataGridViewTextBoxColumn";
            this.eventTypeDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.dgvProcesses);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(1361, 419);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Processes";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.dgvComms);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(1361, 419);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Communications";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.dgvEstimateDetails);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(1361, 419);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "Estimate Details";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // dgvEstimateDetails
            // 
            this.dgvEstimateDetails.AllowUserToAddRows = false;
            this.dgvEstimateDetails.AllowUserToDeleteRows = false;
            this.dgvEstimateDetails.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvEstimateDetails.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvEstimateDetails.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvEstimateDetails.Location = new System.Drawing.Point(3, 3);
            this.dgvEstimateDetails.Name = "dgvEstimateDetails";
            this.dgvEstimateDetails.Size = new System.Drawing.Size(1355, 413);
            this.dgvEstimateDetails.TabIndex = 0;
            // 
            // dataGridViewTextBoxColumn15
            // 
            this.dataGridViewTextBoxColumn15.DataPropertyName = "communicationType";
            this.dataGridViewTextBoxColumn15.HeaderText = "communicationType";
            this.dataGridViewTextBoxColumn15.Name = "dataGridViewTextBoxColumn15";
            this.dataGridViewTextBoxColumn15.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn16
            // 
            this.dataGridViewTextBoxColumn16.DataPropertyName = "communicationText";
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewTextBoxColumn16.DefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridViewTextBoxColumn16.HeaderText = "communicationText";
            this.dataGridViewTextBoxColumn16.Name = "dataGridViewTextBoxColumn16";
            this.dataGridViewTextBoxColumn16.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn17
            // 
            this.dataGridViewTextBoxColumn17.DataPropertyName = "communicationBy";
            this.dataGridViewTextBoxColumn17.HeaderText = "communicationBy";
            this.dataGridViewTextBoxColumn17.Name = "dataGridViewTextBoxColumn17";
            this.dataGridViewTextBoxColumn17.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn18
            // 
            this.dataGridViewTextBoxColumn18.DataPropertyName = "communicationDateTime";
            this.dataGridViewTextBoxColumn18.HeaderText = "communicationDateTime";
            this.dataGridViewTextBoxColumn18.Name = "dataGridViewTextBoxColumn18";
            this.dataGridViewTextBoxColumn18.ReadOnly = true;
            // 
            // frmShowIncidents
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1369, 665);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.dgvFNOL);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frmShowIncidents";
            this.Text = "frmShowIncidents";
            this.Load += new System.EventHandler(this.frmShowIncidents_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dsFNOL)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fNOLBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvFNOL)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsFNOLExternalCommunications)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fNOLExternalCommunicationBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsProcesses)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fNOLProcessesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsFNOLEvents)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vwFNOLEventsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvProcesses)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvComms)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvEvents)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.tabPage3.ResumeLayout(false);
            this.tabPage4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvEstimateDetails)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private dsFNOL dsFNOL;
        private System.Windows.Forms.BindingSource fNOLBindingSource;
        private dsFNOLTableAdapters.FNOLTableAdapter fNOLTableAdapter;
        private dsFNOLTableAdapters.TableAdapterManager tableAdapterManager;
        private System.Windows.Forms.DataGridView dgvFNOL;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn10;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn11;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn12;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn13;
        private dsFNOLExternalCommunications dsFNOLExternalCommunications;
        private System.Windows.Forms.BindingSource fNOLExternalCommunicationBindingSource;
        private dsFNOLExternalCommunicationsTableAdapters.FNOLExternalCommunicationTableAdapter fNOLExternalCommunicationTableAdapter;
        private dsFNOLExternalCommunicationsTableAdapters.TableAdapterManager tableAdapterManager2;
        private dsProcesses dsProcesses;
        private System.Windows.Forms.BindingSource fNOLProcessesBindingSource;
        private dsProcessesTableAdapters.FNOLProcessesTableAdapter fNOLProcessesTableAdapter;
        private dsProcessesTableAdapters.TableAdapterManager tableAdapterManager3;
        private dsCustomersTableAdapters.TableAdapterManager tableAdapterManager4;
        private dsFNOLEvents dsFNOLEvents;
        private System.Windows.Forms.BindingSource vwFNOLEventsBindingSource;
        private dsFNOLEventsTableAdapters.vw_FNOLEventsTableAdapter vw_FNOLEventsTableAdapter;
        private System.Windows.Forms.DataGridView dgvProcesses;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn20;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn21;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn22;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn23;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn24;
        private System.Windows.Forms.DataGridView dgvComms;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.DataGridView dgvEvents;
        private System.Windows.Forms.DataGridViewTextBoxColumn eventDateTimeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn eventTypeDataGridViewTextBoxColumn;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.DataGridView dgvEstimateDetails;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn15;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn16;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn17;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn18;
    }
}