﻿using System;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Windows.Forms;

namespace SMART
{
    public partial class frmNewFNOL : Form
    {
        private int currentRow;
        private Color defaultColour = Color.White;
        private Color errorColour = Color.LemonChiffon;

        public frmNewFNOL()
        {
            InitializeComponent();
            
        }

        private void fNOLBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.fNOLBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.dsFNOL);

        }

        private void frmNewFNOL_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'dsEventTypes.vwEventTypes' table. You can move, or remove it, as needed.
            this.vwEventTypesTableAdapter.Fill(this.dsEventTypes.vwEventTypes);
            // TODO: This line of code loads data into the 'dsEventTypes.vwEventTypes' table. You can move, or remove it, as needed.
            this.vwEventTypesTableAdapter.Fill(this.dsEventTypes.vwEventTypes);
            // TODO: This line of code loads data into the 'dsCustomers.vw_Customers' table. You can move, or remove it, as needed.
            this.vw_CustomersTableAdapter.Fill(this.dsCustomers.vw_Customers);
            // TODO: This line of code loads data into the 'dsRegistrations.vwRegistrations' table. You can move, or remove it, as needed.
            this.vwRegistrationsTableAdapter.Fill(this.dsRegistrations.vwRegistrations);
            // TODO: This line of code loads data into the 'dsFNOL.FNOL' table. You can move, or remove it, as needed.
            this.fNOLTableAdapter.Fill(this.dsFNOL.FNOL);

            fNOLBindingSource.AddNew();
        }

        private void button1_Click(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click_2(object sender, EventArgs e)
        {

            fNOLBindingSource.AddNew();
            wizardControl1.SetNextPage();

        }

        private void wizardControl1_Click(object sender, EventArgs e)
        {

        }

        private void completionWizardPage1_Click(object sender, EventArgs e)
        {
            fNOLBindingSource.EndEdit();
            tableAdapterManager.UpdateAll(this.dsFNOL);
            DataRowView dr = (DataRowView)fNOLBindingSource.Current;
            currentRow = (int)dr["id"];
            createXML(currentRow);
        }

        private void wizardPage1_Click(object sender, EventArgs e)
        {
            fNOLBindingSource.EndEdit();
            tableAdapterManager.UpdateAll(this.dsFNOL);

        }

        private void wizardControl1_NextClick(object sender, DevExpress.XtraWizard.WizardCommandButtonClickEventArgs e)
        {
            //fNOLBindingSource.EndEdit();
            //tableAdapterManager.UpdateAll(this.dsFNOL);
        }

        private void cbRegNotFound_CheckedChanged(object sender, EventArgs e)
        {
            registrationFreeTextEdit.Enabled = cbRegNotFound.Checked;
            registrationTextEdit.Enabled = !cbRegNotFound.Checked;
        }

        private void wizardControl1_SelectedPageChanging(object sender, DevExpress.XtraWizard.WizardPageChangingEventArgs e)
        {
            if (wizardControl1.SelectedPageIndex == 0) //set default for page 2
            {
                dateOfIncidentDateEdit.EditValue = DateTime.Now.ToShortDateString();
                timeofIncidentTimeEdit.EditValue = DateTime.Now.ToShortTimeString();
            }
            fNOLBindingSource.EndEdit();
            tableAdapterManager.UpdateAll(this.dsFNOL);
        }

        /* Data validation below */
        private void driverPhoneNumberSpinEdit_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar >= '0' && e.KeyChar <= '9') || (e.KeyChar == 8) || (e.KeyChar == (char)Keys.Tab))
            {
                return;
            }
            else
            {
                e.Handled = true;
            }
        }

        private void registrationTextEdit_Validating(object sender, CancelEventArgs e)
        {
            if (!cbRegNotFound.Checked)
            {
                if (registrationTextEdit.Text == "")
                {
                    registrationTextEdit.BackColor = errorColour;
                    e.Cancel = true;
                }
                else
                {
                    registrationTextEdit.BackColor = defaultColour;
                    registrationFreeTextEdit.BackColor = defaultColour;
                }
            }
        }

        private void driverPhoneNumberSpinEdit_Validating(object sender, CancelEventArgs e)
        {
            if (driverPhoneNumberSpinEdit.Text == "")
            {
                driverPhoneNumberSpinEdit.BackColor = errorColour;
                e.Cancel = true;
            }
            else
            {
                driverPhoneNumberSpinEdit.BackColor = defaultColour;
            }
        }

        private void customerIDSpinEdit_Validating(object sender, CancelEventArgs e)
        {
            if (customerIDSpinEdit.Text == "")
            {
                customerIDSpinEdit.BackColor = errorColour;
                e.Cancel = true;
            }
            else
            {
                customerIDSpinEdit.BackColor = defaultColour;
            }
        }

        private void registrationFreeTextEdit_Validating(object sender, CancelEventArgs e)
        {
            if (cbRegNotFound.Checked)
            {
                if (registrationFreeTextEdit.Text == "")
                {
                    registrationFreeTextEdit.BackColor = errorColour;
                    e.Cancel = true;
                }
                else
                {
                    registrationTextEdit.BackColor = defaultColour;
                    registrationFreeTextEdit.BackColor = defaultColour;
                }
            }
        }

        private void wizardControl1_FinishClick(object sender, CancelEventArgs e)
        {
            fNOLBindingSource.EndEdit();
            tableAdapterManager.UpdateAll(this.dsFNOL);
            DataRowView dr = (DataRowView)fNOLBindingSource.Current;
            currentRow = (int)dr["id"];

            tableAdapterManager.UpdateAll(this.dsFNOL);
            createXML(currentRow);
            this.Close();
        }

        private void wizardControl1_CancelClick(object sender, CancelEventArgs e)
        {
            DataRowView dr = (DataRowView)fNOLBindingSource.Current;
            dr.Delete();
            this.fNOLBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.dsFNOL);
            this.Close();
        }

        private void is3rdPartyInvolvedCheckEdit_CheckedChanged(object sender, EventArgs e)
        {
            if (is3rdPartyInvolvedCheckEdit.Checked == true)
            {
                no3rdPartyVehiclesSpinEdit.ReadOnly = false;
                no3rdPartyVehiclesSpinEdit.EditValue = 1;
            }
            else
            {
                no3rdPartyVehiclesSpinEdit.EditValue = 0;
                no3rdPartyVehiclesSpinEdit.ReadOnly = true;
            }
        }

        private void isDriverInjuredCheckEdit_CheckedChanged(object sender, EventArgs e)
        {
            driverInjuryDetailsTextEdit.Enabled = isDriverInjuredCheckEdit.Checked;
            treatmentReceivedCheckEdit.Enabled = isDriverInjuredCheckEdit.Checked;
            isHospitalNeededCheckEdit.Enabled = isDriverInjuredCheckEdit.Checked;
            hospitalDetailsTextEdit.Enabled = isDriverInjuredCheckEdit.Checked;
        }
    }

}